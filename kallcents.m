%find k mean centers
function [idx, c, labeled, namepts] = kallcents(pathname, filename, k, skr, m)
    %c;
    %labeled would contain indices where the emotion changes in the doc
    %namepts contain indices where the name changes
    %filen = dir(fullfile(pathname, filename));
    %m = 0 =>mask with zeros & m = 1 =>mask with corresponding means
    [num, txt, all] = xlsread([pathname filename]);
    
    %%%%%%% mask NaN with 0 %%%%%%%%%%%
    if (m == 0)
    mask = isnan(num);
    num(mask) = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% mask NaN with mean %%%%%%%%%%
    if (m ~= 0)
    maskmean = nanmean(num);
    for ii = 1:size(num, 2)
        maskk = isnan(num(:, ii));
        num(maskk, ii) = maskmean(ii);
    end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    namefeats = txt(1, 1:end); % name of all the features in cell
    names = txt(2:end, 1);      % contains the names of all the files from which a small sample belongs to
    namepts = ones(1,1);
    labels = txt(2:end, 2);     %stores all the emotion labels
    labeled = ones(1, 6);       %indices where labels change
    i = 2;
    j = 2;
    prevname = cell2mat(names(1));
    prev = cell2mat(labels(1));
    while (i < length(labels))
        currentname = cell2mat(names(i));
        current = cell2mat(labels(i));
        if strcmp(current, prev) ~= 1
            labeled(j) = i - 1;
            j = j + 1;
        end
        if strcmp(currentname, prevname) ~= 1
            namepts = vertcat(namepts, i-1);
        end
        prev = current;
        prevname = currentname;
        i = i + 1;
    end
    
    temp = num;
    id =  find(isnan(num));
    temp(id) = 0;
    means = mean(temp);

    %Make the NaN values in num equal to the mean value of the column
    ll = 1;
    jj = 1;
    while (jj <= size(num, 2))
       idd = find(isnan(num(1:end, jj)));
       num(idd, jj) = means(jj);
       jj = jj+1;
    end
    
    a = num(1:(labeled(1)-1), :);
    b = num(labeled(1) + 1: (labeled(2)), :);
    cc = num(labeled(2) + 1: (labeled(3)), :);
    d = num(labeled(3) + 1: (labeled(4)), :);
    e = num(labeled(4) + 1: (labeled(5)), :);
    f = num(labeled(5) + 1: (labeled(6)), :);
    if (skr > 1) %skip ratio in the data samples to choose some random samples
        idx = 0;
        c = 0;
        return;
    end
    a = datasample(a, floor(skr*(length(a))));         %select random datasamples from data
    b = datasample(b, floor(skr*(length(b))));         
    cc = datasample(cc, floor(skr*(length(cc))));
    d = datasample(d, floor(skr*(length(d))));
    e = datasample(e, floor(skr*(length(e))));
    f = datasample(f, floor(skr*(length(f))));
    data = vertcat(a, b, cc, d, e, f);
    [idx, c] = kmeans(data, k, 'distance','cosine','start','sample', 'emptyaction','singleton');
    while length(unique(idx))< k ||  histc(histc(idx,1:k),1)~=0
        % i.e. while one of the clusters is empty -- or -- we have one or more clusters with only one member
        [idx, c] = kmeans(data, k, 'distance','cosine','start','sample', 'emptyaction','singleton');
    end  
end