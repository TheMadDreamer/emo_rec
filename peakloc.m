function [P,W]= peakloc(y)
% Written by Weiyang Cai, University of Rochester.
% P is the amplitude of valley, W1 shows the index of the valley
L = length(y);
P = ones(1,L);
for i = 2:(L-1)
    if y(i)>y(i-1) && y(i)> y(i+1)
        P(i) = y(i);
    end
end
W = find(P~=1);