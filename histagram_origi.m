%histogram plot
function [svmodel] = histagram(pathname, filename, k, skr)
    [idx, c, labeled, namepts] = kcents(pathname, filename, k, skr);
    groupes = 1:k;
    %s = sprintf('k is %d', k);
    %ct = zscore(c);
    %disp(c);
    minimums = min(c, [], 1);
    ranges = max(c, [], 1) - minimums;
    c = (c - repmat(minimums, size(c, 1), 1)) ./ repmat(ranges, size(c, 1), 1);
    disp(c);
    %test_data = (test_data - repmat(minimums, size(test_data, 1), 1)) ./ repmat(ranges, size(test_data, 1), 1);
    svmstruct = svmtrain(c, groupes');
    pre = svmpredict(groupes', c, svmstruct);
    [num, txt] = xlsread([pathname filename]);
    disp(pre);

    his = ones(length(namepts), k);
    groups = ones(length(namepts)-1, 1);
    count_for_labels = 1;
    for i = 2:length(namepts)
        %predict the classes (for frequency) for each file
        if (i ~= length(namepts))
            classes = svmpredict(ones(-namepts(i, 1)+namepts(i+1, 1), 1), num(namepts(i, 1):(namepts(i+1, 1) - 1), 1:end), svmstruct);
            %disp(classes);
        else
            classes = svmpredict(ones(size(num, 1) - namepts(i)+1, 1), num(namepts(i):size(num, 1), 1:end), svmstruct);
        end
        %make histogram for all files
        for ll = 1:length(classes)
            if (classes(ll) <= k)
                his(i, classes(ll)) = his(i, classes(ll)) + 1;
            end
        end

        if (count_for_labels < 6)
                if (namepts(i) == labeled(count_for_labels + 1))
                    count_for_labels = count_for_labels + 1;
                end
        end
        groups(i) = count_for_labels;
    end
    svmodel = svmtrain(his, groups, '-c 1000 -g 0.2');
    %disp(groups);
    s = svmpredict(groups, his, svmodel);
    %disp(s);
end
%filesi = {1};
    % Convert from cell to char array
    %names = txt(2:end, 1);
    %prev = cell2mat(names(1));
    %i = 2; j = 1;
    %while (i < length(names))
    %    current = cell2mat(names(i));
    %    if strcmp(prev, current) ~= 1
    %        filesi = vertcat(filesi, i);
    %        j = j + 1;
    %    end
    %    prev = current;
    %   i = i + 1;
    %end
    %%%%%%
    