% Generate cross-validation datasets for different testing scenarios
%
% See the Bridge project website at Wireless Communication and Networking
% Group (WCNG), University of Rochester for more information: 
% http://www.ece.rochester.edu/projects/wcng/project_bridge.html
%
% Written by Na Yang, University of Rochester.
% Copyright (c) 2013 University of Rochester.
% Version: October 2013.
%
% Permission to use, copy, modify, and distribute this software without 
% fee is hereby granted FOR RESEARCH PURPOSES only, provided that this
% copyright notice appears in all copies and in all supporting 
% documentation, and that the software is not redistributed for any fee.
%
% For any other uses of this software, in original or modified form, 
% including but not limited to consulting, production or distribution
% in whole or in part, specific prior permission must be obtained from WCNG.
% Algorithms implemented by this software may be claimed by patents owned 
% by WCNG, University of Rochester.
%
% The WCNG makes no representations about the suitability of this 
% software for any purpose. It is provided "as is" without express
% or implied warranty.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Stratified cross-validation. If cross=7, 6/7 of the data is used for 
% training, and the rest of the 1/7 data is used for testing for each round. 
% Uneven one-against-all (OAA) training set contains all the positives and 
% negatives. Even OAA training set contains all the positives, and the same
% number of randomly selected negatives.
%
% Inputs:
% whole_xls -> an Excel file containing all the data to be divided for
% training and testing. Choose the according file for extracted features 
% with or without speaker normalization
%
% folder -> name of the folder containing all the generated datasets
%
% emotion -> emotion categories, consistent with those in the audio names
%
% Outputs:
% Generated cross-validation datasets for different testing scenarios,
% including the general test, gender-dependent test, and test using even or
% uneven datasets.
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Parameter settings
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cross = 7; % number of rounds of cross-validations
whole_xls = '../Excels/Feature_LDC_Clean_norm.xls'; % Data to be divided for training and testing
folder = '../Datasets/Dataset_LDC_Clean_norm/'; % folder containing all the generated datasets
emotion = {'disgust', 'happy', 'hotAnger', 'sadness', 'neutral', 'panic'}; % emotion categories, consistent with those in the audio names

% create new folders
mkdir(folder);
mkdir(folder, 'test');
mkdir(folder, 'train_all');
% mkdir(folder, 'train_even');
mkdir(folder, 'train_uneven');
mkdir(folder, 'test_male');
mkdir(folder, 'test_female');
% mkdir(folder, 'train_even_male');
% mkdir(folder, 'train_even_female');
mkdir(folder, 'train_uneven_male');
mkdir(folder, 'train_uneven_female');

% gather row numbers belonging to the same emotion
[num, txt, raw]=xlsread(whole_xls);
audioname = txt(2:size(txt,1),1);
disgust_ori = [];
happy_ori = [];
angry_ori = [];
sad_ori = [];
neutral_ori = [];
fear_ori = [];
for i = 1:size(num,1) % i = row-1
    switch num(i,1)
        case 1
            disgust_ori(end+1) = i;
        case 2
            happy_ori(end+1) = i;
        case 3
            angry_ori(end+1) = i;
        case 4
            sad_ori(end+1) = i;
        case 5
            neutral_ori(end+1) = i;
        case 6
            fear_ori(end+1) = i;
    end
end

% shuffle row numbers within each emotion
shuffled{1,:} = disgust_ori(randperm(length(disgust_ori)));
shuffled{2,:} = happy_ori(randperm(length(happy_ori)));
shuffled{3,:} = angry_ori(randperm(length(angry_ori)));
shuffled{4,:} = sad_ori(randperm(length(sad_ori)));
shuffled{5,:} = neutral_ori(randperm(length(neutral_ori)));
shuffled{6,:} = fear_ori(randperm(length(fear_ori)));

% stratified partition for cross-validation
partition = cell(6,cross);
for i=1:6
    for j=1:length(shuffled{i,:})
        c = mod(j,cross)+1; % c=1,...,cross
        partition{i,c}(end+1) = shuffled{i,1}(j);
    end
end

for c=1:cross % for each round of cross-validation
    % build one testing data Excel
    [num, txt, raw]=xlsread(whole_xls);
    audioname = txt(2:size(txt,1),1);
    test_rows = [];
    for i=1:6
        test_rows = [test_rows partition{i,c}];
    end
    testxls = [folder 'test\test_' num2str(c) '.xls'];
    delete(testxls);
    xlswrite(testxls, txt(1,:), 'Sheet1'); % write title row
    xlswrite(testxls, audioname(test_rows,:), 'Sheet1', 'A2');
    xlswrite(testxls, num(test_rows,:), 'Sheet1', 'B2');
    
    % testing data for male and female
    test_rows_male = [];
    test_rows_female = [];
    for myr = 1:length(test_rows)
        if num(test_rows(myr),3) == 1
            test_rows_male(end+1) = test_rows(myr);
        else
            test_rows_female(end+1) = test_rows(myr);
        end
    end
    
    testxls_male = [folder 'test_male\test_' num2str(c) '_male.xls'];
    delete(testxls_male);
    xlswrite(testxls_male, txt(1,:), 'Sheet1'); % write title row
    xlswrite(testxls_male, audioname(test_rows_male,:), 'Sheet1', 'A2');
    xlswrite(testxls_male, num(test_rows_male,:), 'Sheet1', 'B2');
    
    testxls_female = [folder 'test_female\test_' num2str(c) '_female.xls'];
    delete(testxls_female);
    xlswrite(testxls_female, txt(1,:), 'Sheet1'); % write title row
    xlswrite(testxls_female, audioname(test_rows_female,:), 'Sheet1', 'A2');
    xlswrite(testxls_female, num(test_rows_female,:), 'Sheet1', 'B2');
    
        
    % build one training data Excel using the rest of the data    
    train_all_xls = [folder 'train_all\train_' num2str(c) '_all.xls'];
    train_rows = setdiff(1:size(num,1),test_rows);
    delete(train_all_xls);
    xlswrite(train_all_xls, txt(1,:), 'Sheet1'); % write title row
    xlswrite(train_all_xls, audioname(train_rows,:), 'Sheet1', 'A2');
    xlswrite(train_all_xls, num(train_rows,:), 'Sheet1', 'B2');
    
    % build 6 training data Excels with even number of 1s and 2s
    for e=1:6
        % step 1: change emotion label to 1s and 2s (uneven data)
        train_uneven_xls = [folder 'train_uneven\train_' num2str(c) '_' emotion{e} '_AgainstAll_uneven.xls'];
        copyfile(train_all_xls,train_uneven_xls);
        [num, txt, ~]=xlsread(train_uneven_xls);
        audioname = txt(2:size(txt,1),1);
        parts = regexp(audioname, '_|\.', 'split');
        label = ones(length(parts),1)*2;
        for line=1:length(parts);
            one_emotion=strcat(parts{line}{3});
            if strcmp(one_emotion, emotion{e})
                label(line) = 1;    
            end;
        end
        delete(train_uneven_xls);
        xlswrite(train_uneven_xls, txt(1,:), 'Sheet1'); % write title row
        xlswrite(train_uneven_xls, audioname, 'Sheet1', 'A2'); % write audiofile names
        xlswrite(train_uneven_xls, label, 'Sheet1', 'B2'); % write emotion label 1 or 2
        xlswrite(train_uneven_xls, num(:,3:end), 'Sheet1', 'D2'); 
        
        % uneven training data for male and female
        train_uneven_xls_male = [folder 'train_uneven_male\train_' num2str(c) '_' emotion{e} '_AgainstAll_uneven_male.xls'];
        delete(train_uneven_xls_male);
        xlswrite(train_uneven_xls_male, txt(1,:), 'Sheet1'); % write title row
        xlswrite(train_uneven_xls_male, audioname(num(:,3)==1,:), 'Sheet1', 'A2'); % write audiofile names
        xlswrite(train_uneven_xls_male, label(num(:,3)==1,:), 'Sheet1', 'B2'); % write emotion label 1 or 2
        xlswrite(train_uneven_xls_male, num((num(:,3)==1),3:end), 'Sheet1', 'D2'); 
        
        train_uneven_xls_female = [folder 'train_uneven_female\train_' num2str(c) '_' emotion{e} '_AgainstAll_uneven_female.xls'];
        delete(train_uneven_xls_female);
        xlswrite(train_uneven_xls_female, txt(1,:), 'Sheet1'); % write title row
        xlswrite(train_uneven_xls_female, audioname(num(:,3)==2,:), 'Sheet1', 'A2'); % write audiofile names
        xlswrite(train_uneven_xls_female, label(num(:,3)==2,:), 'Sheet1', 'B2'); % write emotion label 1 or 2
        xlswrite(train_uneven_xls_female, num((num(:,3)==2),3:end), 'Sheet1', 'D2'); 
        
        
%         % step 2: even number of 1s and 2s
%         [num, txt, raw]=xlsread(train_uneven_xls);
%         audioname = txt(2:size(txt,1),1);
%         filename=txt(2:size(txt,1),1);
%         parts = regexp(filename, '_|\.', 'split');
%         a={}; % (speaker index, file index), size(a,1) = size(num,1)
%         d=[]; % number of files for each speaker (1s & 2s)
%         n=''; % current speaker name
%         k=0; % index of speaker
%         j=0; % index of files for one speaker
%         for i=1:length(parts)
%             name=strcat(parts{i}{1:2}); % choose the same speaker name
%             if ~strcmp(name, n)
%                 k=k+1;
%                 j=0;
%                 d(k)=0;
%             end
%             j=j+1;
%             d(k)=d(k)+1;
%             a{length(a)+1}=[k, j];
%             n=name;
%         end
%         p=zeros(1,length(a));
%         for speaker=1:length(d)
%             if speaker==1
%                 beginLine = 1;
%                 endLine = d(1);
%             else
%                 beginLine = endLine+1;
%                 endLine = endLine+d(speaker);
%             end;
%             v1 = find(num(beginLine:endLine,1)==1);
%             v1_lines = beginLine+v1-1;
%             Num_ones = length(v1);
%             v2 = find(num(beginLine:endLine,1)~=1);
%             r = randperm(length(v2));
%             v2_kept = v2(r(1:Num_ones));
%             v2_lines_kept = beginLine+v2_kept-1;
% 
%             p(v1_lines)=100;
%             p(v2_lines_kept)=100;    
%         end
%         train_even_xls = [folder 'train_even\train_' num2str(c) '_' emotion{e} '_AgainstAll_even.xls'];
%         delete(train_even_xls);
%         xlswrite(train_even_xls, txt(1,:), 'Sheet1'); % write title row
%         xlswrite(train_even_xls, audioname(p==100,:), 'Sheet1', 'A2'); % write audiofile names
%         xlswrite(train_even_xls, num(p==100,:), 'Sheet1', 'B2');
%         
%         % even training data for male and female
%         train_even_rows_male = [];
%         train_even_rows_female = [];
%         for i=1:length(parts)
%             if p(i)==100 && num(i,3) == 1
%                 train_even_rows_male(end+1) = i;
%             elseif p(i)==100 && num(i,3) == 2
%                 train_even_rows_female(end+1) = i;
%             end
%         end
%     
%         train_even_xls_male = [folder 'train_even_male\train_' num2str(c) '_' emotion{e} '_AgainstAll_even_male.xls'];
%         delete(train_even_xls_male);
%         xlswrite(train_even_xls_male, txt(1,:), 'Sheet1'); % write title row
%         xlswrite(train_even_xls_male, audioname(train_even_rows_male,:), 'Sheet1', 'A2'); % write audiofile names
%         xlswrite(train_even_xls_male, num(train_even_rows_male,:), 'Sheet1', 'B2');
% 
%         train_even_xls_female = [folder 'train_even_female\train_' num2str(c) '_' emotion{e} '_AgainstAll_even_female.xls'];
%         delete(train_even_xls_female);
%         xlswrite(train_even_xls_female, txt(1,:), 'Sheet1'); % write title row
%         xlswrite(train_even_xls_female, audioname(train_even_rows_female,:), 'Sheet1', 'A2'); % write audiofile names
%         xlswrite(train_even_xls_female, num(train_even_rows_female,:), 'Sheet1', 'B2');
    end
end
