function [Thresh_accu, recall_emotion, num_emotion] = OnefoldCV(trainfilepath, testfilepath,...
    feature, thresh, dataset, cnt, subject)
% % Perform one fold cross validation for the speech-based emotion
% classification with upsampling
%
% Input arguments:
% trainfilepath - path where the training data is located
% testfilepath - path where the test data is located and filename.xls format
% feature - selected based on mutual information
% thresh - absolute confidence threshold
% dataset - select using even or uneven dataset
% cnt - specify which fold of cross validation or which speaker (for LOSO test) we are running
% subject - if specified, it's for LOSO test
%
% Output:
% Thresh_accu - three rows to store, 1 - absolute threshold, 2 - rejection rate,
% 3 - correct classification rate
% recall_emotion - DL recall for each individual emotion (anger, sadness, disgust, neutral
% happiness, fear)
%
% Modified based on Rajani Muraleedharan's original code, written by 
% Jiaobo Yuan, Yun Zhou University of Rochester.
% Permission to use, copy, modify, and distribute this software without
% fee is hereby granted FOR RESEARCH PURPOSES only, provided that this
% copyright notice appears in all copies and in all supporting
% documentation, and that the software is not redistributed for any fee.
%
% For any other uses of this software, in original or modified form,
% including but not limited to consulting, production or distribution
% in whole or in part, specific prior permission must be obtained from WCNG.
% Algorithms implemented by this software may be claimed by patents owned
% by WCNG, University of Rochester.
%
% The WCNG makes no representations about the suitability of this
% software for any purpose. It is provided "as is" without express
% or implied warranty.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m = 5; % upsample 5 times
no_of_class = 6;
% Kernel settings: rbf
kernel1 ='rbf';% (1) - HAPPY
kernel2 ='rbf';% (2) - SADNESS
kernel3 ='rbf';% (3) - FEAR
kernel4 ='rbf';% (4) - ANGER
kernel5 ='rbf';% (5) - DISGUST
kernel6 ='rbf';% (6) - NEUTRAL

rbf_sigma = 5;
% Optimization Method - SMO
options = svmsmoset('maxiter',500000);

% load dataset
% filename	ActivePassive	PositiveNegative	Gender	mean energy	std energy	max energy	min energy	range energy
% mean energy diff	std energy diff	max energy diff	min energy diff	range energy diff	mean formant1	std formant1
% max formant1	min formant1	range formant1	mean formant2	std formant2	max formant2	min formant2
% range formant2	mean formant3	std formant3	max formant3	min formant3	range formant3	mean formant4
% std formant4	max formant4	min formant4	range formant4	mean formant1 bw	std formant1 bw	max formant1 bw
% min formant1 bw	range formant1 bw	mean formant2 bw	std formant2 bw	max formant2 bw	min formant2 bw	range formant2 bw
% mean formant3 bw	std formant3 bw	max formant3 bw	min formant3 bw	range formant3 bw	mean formant4 bw	std formant4 bw
% max formant4 bw	min formant4 bw	range formant4 bw	mean pitch	std pitch	max pitch	min pitch	range pitch
% mean pitch diff	std pitch diff	max pitch diff	min pitch diff	range pitch diff	mean M1	std M1	max M1	min M1
% range M1	mean M2	std M2	max M2	min M2	range M2	mean M3	std M3	max M3	min M3	range M3	mean M4	std M4	max M4
% min M4	range M4	mean M5	std M5	max M5	min M5	range M5	mean M6	std M6	max M6	min M6	range M6	mean M7	std M7
% max M7	min M7	range M7	mean M8	std M8	max M8	min M8	range M8	mean M9	std M9	max M9	min M9	range M9	mean M10
% std M10	max M10	min M10	range M10	mean M11	std M11	max M11	min M11	range M11	mean M12	std M12	max M12	min M12	range M12	SRate

%% Read dataset
if nargin == 6  % for cross validation
    % TRAINING
    % HAPPY EMOTION
    numbers_happy = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_happy_AgainstAll_' dataset '.xls']));
    % SAD EMOTION
    numbers_sad = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_sadness_AgainstAll_' dataset '.xls']));
    % FEAR EMOTION
    numbers_fear = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_panic_AgainstAll_' dataset '.xls']));
    % ANGER EMOTION
    numbers_anger = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_hotAnger_AgainstAll_' dataset '.xls']));
    % DISGUST EMOTION
    numbers_disgust = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_disgust_AgainstAll_' dataset '.xls']));
    % NEUTRAL EMOTION
    numbers_neutral = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_neutral_AgainstAll_' dataset '.xls']));

    % TEST
    numbers = xlsread(fullfile(testfilepath, ['test_' num2str(cnt) '.xls']));
else % for LOSO test
    % TRAINING
    % HAPPY EMOTION
    numbers_happy = xlsread(strcat(trainfilepath,['happy_AgainstAll_LOSO_train_' subject{cnt} '.xls']));
    % SAD EMOTION
    numbers_sad = xlsread(strcat(trainfilepath,['sadness_AgainstAll_LOSO_train_' subject{cnt} '.xls']));
    % FEAR EMOTION
    numbers_fear = xlsread(strcat(trainfilepath,['panic_AgainstAll_LOSO_train_' subject{cnt} '.xls']));
    % ANGER EMOTION
    numbers_anger = xlsread(strcat(trainfilepath,['hotAnger_AgainstAll_LOSO_train_' subject{cnt} '.xls']));
    % DISGUST EMOTION
    numbers_disgust = xlsread(strcat(trainfilepath,['disgust_AgainstAll_LOSO_train_' subject{cnt} '.xls']));
    % NEUTRAL EMOTION
    numbers_neutral = xlsread(strcat(trainfilepath,['neutral_AgainstAll_LOSO_train_' subject{cnt} '.xls']));

    % TEST
    numbers = xlsread(strcat(testfilepath,'LOSO_test_',subject{cnt}));
end
    
% The features are read into matrix
sdata_happy = numbers_happy(:,feature);
% Grouped based on Individual Emotion - Happy
sgroup_happy = numbers_happy(:,1);

% The features are read into matrix
sdata_sad = numbers_sad(:,feature);
% Grouped based on Individual Emotion - Sad
sgroup_sad = numbers_sad(:,1);

% The features are read into matrix
sdata_fear = numbers_fear(:,feature);
% Grouped based on Individual Emotion -Fear
sgroup_fear = numbers_fear(:,1); 

% The features are read into matrix
sdata_anger = numbers_anger(:,feature);
% Grouped based on Individual Emotion - Anger
sgroup_anger = numbers_anger(:,1);

% The features are read into matrix
sdata_disgust = numbers_disgust(:,feature);
% Grouped based on Individual Emotion - Disgust
sgroup_disgust = numbers_disgust(:,1);

% The features are read into matrix
sdata_neutral = numbers_neutral(:,feature);
% Grouped based on Individual Emotion - Neutral
sgroup_neutral = numbers_neutral(:,1);

num_f = length(feature);
%% upsampling
[sdata_neutral, sgroup_neutral] = multi(sdata_neutral, sgroup_neutral,m,num_f);
[sdata_disgust, sgroup_disgust] = multi(sdata_disgust, sgroup_disgust,m,num_f);
[sdata_anger, sgroup_anger] = multi(sdata_anger, sgroup_anger,m,num_f);
[sdata_fear, sgroup_fear] = multi(sdata_fear, sgroup_fear,m,num_f);
[sdata_sad, sgroup_sad] = multi(sdata_sad, sgroup_sad,m,num_f);
[sdata_happy, sgroup_happy] = multi(sdata_happy, sgroup_happy,m,num_f);

sgroup = numbers(:,1);
testdata = numbers(:,feature);

%% Train each individual One-Against-All classifiers
svmStructs_happy = svmtrain(sdata_happy,sgroup_happy,'Kernel_Function',kernel1,...
    'Method','SMO','rbf_sigma',rbf_sigma,'smo_opts',options,'autoscale',true);
svmStructs_sad = svmtrain(sdata_sad,sgroup_sad,'Kernel_Function',kernel2,...
    'Method','SMO','rbf_sigma',rbf_sigma,'smo_opts',options,'autoscale',true);
svmStructs_fear = svmtrain(sdata_fear,sgroup_fear,'Kernel_Function',kernel3,...
    'Method','SMO','rbf_sigma',rbf_sigma,'smo_opts',options,'autoscale',true);
svmStructs_anger = svmtrain(sdata_anger,sgroup_anger,'Kernel_Function',kernel4,...
    'Method','SMO','rbf_sigma',rbf_sigma,'smo_opts',options,'autoscale',true);
svmStructs_disgust = svmtrain(sdata_disgust,sgroup_disgust,'Kernel_Function',kernel5,...
    'Method','SMO','rbf_sigma',rbf_sigma,'smo_opts',options,'autoscale',true);
svmStructs_neutral = svmtrain(sdata_neutral,sgroup_neutral,'Kernel_Function',kernel6,...
    'Method','SMO','rbf_sigma',rbf_sigma,'smo_opts',options,'autoscale',true);

% Use the model trained for each SVM classifiers to predict the label and confidence value 
%  for the test instances
[classes_test_happy, confident_test_happy] = svmclassify(svmStructs_happy,testdata);
[classes_test_anger, confident_test_anger] = svmclassify(svmStructs_anger,testdata);
[classes_test_sad, confident_test_sad] = svmclassify(svmStructs_sad,testdata);
[classes_test_fear, confident_test_fear] = svmclassify(svmStructs_fear,testdata);
[classes_test_disgust, confident_test_disgust] = svmclassify(svmStructs_disgust,testdata);
[classes_test_neutral, confident_test_neutral] = svmclassify(svmStructs_neutral,testdata);

classes_emotion_test = [classes_test_disgust classes_test_happy classes_test_anger classes_test_sad classes_test_neutral classes_test_fear];
confident_emotion_test = [confident_test_disgust confident_test_happy confident_test_anger confident_test_sad confident_test_neutral confident_test_fear];
classes_predict_emotion = zeros(size(classes_emotion_test));

%% select absolute confidence threshold for thresholding fusion
cnt_thresh = length(thresh);
for count = 1:cnt_thresh 
    cus_threshold = thresh(count);
    for j = 1:length(sgroup)
        max_thr = sort(confident_emotion_test(j,:),2,'descend'); % Sort the confidence levels in descending order
        max_conf(j) = max_thr(1); % find the highestt absolute confidence
        [a,b] = find(confident_emotion_test==max(confident_emotion_test(j,:)));
        if (max_conf(j)>cus_threshold) % threshold condition
            classes_predict_emotion(a,b)=1; % classes_emotion_test(a,b);
        else
            classes_predict_emotion(a,b)=0;
        end
    end

% According to the database: Unseen data is grouped as
% 1 - Disgust
% 2 - Happy
% 3 - Angry
% 4 - Sad
% 5 - Neutral
% 6 - Fear/Panic

% Formation of Confusion Matrix:
ed_conf_matrix = zeros(no_of_class,no_of_class);
dd=0;dh=0;da=0;ds=0;dn=0;df=0;
hd=0;hh=0;ha=0;hs=0;hn=0;hf=0;
ad=0;ah=0;aa=0;as=0;an=0;af=0;
sd=0;sh=0;sa=0;ss=0;sn=0;sf=0;
nd=0;nh=0;na=0;ns=0;nn=0;nf=0;
fd=0;fh=0;fa=0;fs=0;fn=0;ff=0;

for k=1:length(sgroup)
    if (sgroup(k)==1)
        %  Disgust - detected, Other Emotions - Should be negative
        disgust=1;
        dd=dd+length(find(classes_predict_emotion(k,1)==disgust));
        dh=dh+length(find(classes_predict_emotion(k,2)==disgust));
        da=da+length(find(classes_predict_emotion(k,3)==disgust));
        ds=ds+length(find(classes_predict_emotion(k,4)==disgust));
        dn=dn+length(find(classes_predict_emotion(k,5)==disgust));
        df=df+length(find(classes_predict_emotion(k,6)==disgust));
        
    elseif (sgroup(k)==2)
        %  Happy - detected, Other Emotions - Should be negative
        happy=1;
        hd=hd+length(find(classes_predict_emotion(k,1)==happy));
        hh=hh+length(find(classes_predict_emotion(k,2)==happy));
        ha=ha+length(find(classes_predict_emotion(k,3)==happy));
        hs=hs+length(find(classes_predict_emotion(k,4)==happy));
        hn=hn+length(find(classes_predict_emotion(k,5)==happy));
        hf=hf+length(find(classes_predict_emotion(k,6)==happy));
        
    elseif (sgroup(k)==3) %
        %  Anger - detected, Other Emotions - Should be negative
        anger=1;
        ad=ad+length(find(classes_predict_emotion(k,1)==anger));
        ah=ah+length(find(classes_predict_emotion(k,2)==anger));
        aa=aa+length(find(classes_predict_emotion(k,3)==anger));
        as=as+length(find(classes_predict_emotion(k,4)==anger));
        an=an+length(find(classes_predict_emotion(k,5)==anger));
        af=af+length(find(classes_predict_emotion(k,6)==anger));
        
    elseif (sgroup(k)==4) %
        %  Sad - detected, Other Emotions - Should be negative
        sad=1;
        sd=sd+length(find(classes_predict_emotion(k,1)==sad));
        sh=sh+length(find(classes_predict_emotion(k,2)==sad));
        sa=sa+length(find(classes_predict_emotion(k,3)==sad));
        ss=ss+length(find(classes_predict_emotion(k,4)==sad));
        sn=sn+length(find(classes_predict_emotion(k,5)==sad));
        sf=sf+length(find(classes_predict_emotion(k,6)==sad));
        
    elseif (sgroup(k)==5) %
        %  Sad - detected, Other Emotions - Should be negative
        neutral=1;
        nd=nd+length(find(classes_predict_emotion(k,1)==neutral));
        nh=nh+length(find(classes_predict_emotion(k,2)==neutral));
        na=na+length(find(classes_predict_emotion(k,3)==neutral));
        ns=ns+length(find(classes_predict_emotion(k,4)==neutral));
        nn=nn+length(find(classes_predict_emotion(k,5)==neutral));
        nf=nf+length(find(classes_predict_emotion(k,6)==neutral));
        
    elseif (sgroup(k)==6) %
        %  Sad - detected, Other Emotions - Should be negative
        fear=1;
        fd=fd+length(find(classes_predict_emotion(k,1)==fear));
        fh=fh+length(find(classes_predict_emotion(k,2)==fear));
        fa=fa+length(find(classes_predict_emotion(k,3)==fear));
        fs=fs+length(find(classes_predict_emotion(k,4)==fear));
        fn=fn+length(find(classes_predict_emotion(k,5)==fear));
        ff=ff+length(find(classes_predict_emotion(k,6)==fear));
    end
    
end

ed_conf_matrix(1,1)=dd; ed_conf_matrix(1,2)=dh; ed_conf_matrix(1,3)=da; ed_conf_matrix(1,4)=ds; ed_conf_matrix(1,5)=dn; ed_conf_matrix(1,6)=df;
ed_conf_matrix(2,1)=hd; ed_conf_matrix(2,2)=hh; ed_conf_matrix(2,3)=ha; ed_conf_matrix(2,4)=hs; ed_conf_matrix(2,5)=hn; ed_conf_matrix(2,6)=hf;
ed_conf_matrix(3,1)=ad; ed_conf_matrix(3,2)=ah; ed_conf_matrix(3,3)=aa; ed_conf_matrix(3,4)=as; ed_conf_matrix(3,5)=an; ed_conf_matrix(3,6)=af;
ed_conf_matrix(4,1)=sd; ed_conf_matrix(4,2)=sh; ed_conf_matrix(4,3)=sa; ed_conf_matrix(4,4)=ss; ed_conf_matrix(4,5)=sn; ed_conf_matrix(4,6)=sf;
ed_conf_matrix(5,1)=nd; ed_conf_matrix(5,2)=nh; ed_conf_matrix(5,3)=na; ed_conf_matrix(5,4)=ns; ed_conf_matrix(5,5)=nn; ed_conf_matrix(5,6)=nf;
ed_conf_matrix(6,1)=fd; ed_conf_matrix(6,2)=fh; ed_conf_matrix(6,3)=fa; ed_conf_matrix(6,4)=fs; ed_conf_matrix(6,5)=fn; ed_conf_matrix(6,6)=ff;

% Calculating True Positives
tp_disgust = ed_conf_matrix(1,1);
tp_happy = ed_conf_matrix(2,2);
tp_anger = ed_conf_matrix(3,3);
tp_sad = ed_conf_matrix(4,4);
tp_neutral = ed_conf_matrix(5,5);
tp_fear = ed_conf_matrix(6,6);

% Calculating False Positives
fp_disgust = sum(ed_conf_matrix(:,1))-ed_conf_matrix(1,1);
fp_happy = sum(ed_conf_matrix(:,2))-ed_conf_matrix(2,2);
fp_anger = sum(ed_conf_matrix(:,3))-ed_conf_matrix(3,3);
fp_sad = sum(ed_conf_matrix(:,4))-ed_conf_matrix(4,4);
fp_neutral = sum(ed_conf_matrix(:,5))-ed_conf_matrix(5,5);
fp_fear = sum(ed_conf_matrix(:,6))-ed_conf_matrix(6,6);

% Calculating False Negatives
fn_disgust = sum(ed_conf_matrix(1,:))-ed_conf_matrix(1,1);
fn_happy = sum(ed_conf_matrix(2,:))-ed_conf_matrix(2,2);
fn_anger = sum(ed_conf_matrix(3,:))-ed_conf_matrix(3,3);
fn_sad = sum(ed_conf_matrix(4,:))-ed_conf_matrix(4,4);
fn_neutral = sum(ed_conf_matrix(5,:))-ed_conf_matrix(5,5);
fn_fear = sum(ed_conf_matrix(6,:))-ed_conf_matrix(6,6);

% Calculating True Negatives
tn_disgust = sum(sum(ed_conf_matrix))-sum(ed_conf_matrix(:,1))-sum(ed_conf_matrix(1,:))+ed_conf_matrix(1,1);
tn_happy = sum(sum(ed_conf_matrix))-sum(ed_conf_matrix(:,2))-sum(ed_conf_matrix(2,:))+ed_conf_matrix(2,2);
tn_anger = sum(sum(ed_conf_matrix))-sum(ed_conf_matrix(:,3))-sum(ed_conf_matrix(3,:))+ed_conf_matrix(3,3);
tn_sad = sum(sum(ed_conf_matrix))-sum(ed_conf_matrix(:,4))-sum(ed_conf_matrix(4,:))+ed_conf_matrix(4,4);
tn_neutral = sum(sum(ed_conf_matrix))-sum(ed_conf_matrix(:,5))-sum(ed_conf_matrix(5,:))+ed_conf_matrix(5,5);
tn_fear = sum(sum(ed_conf_matrix))-sum(ed_conf_matrix(:,6))-sum(ed_conf_matrix(6,:))+ed_conf_matrix(6,6);

%% Performance Measures
% Number of unclassified instances in the whole set
unclassified_emotion = length(find((classes_predict_emotion(:,1)==0)&...
    (classes_predict_emotion(:,2)==0)&(classes_predict_emotion(:,3)==0)&...
    (classes_predict_emotion(:,4)==0)&(classes_predict_emotion(:,5)==0)&...
    (classes_predict_emotion(:,6)==0)));
unclas_rate_emotion = ((unclassified_emotion./length(sgroup)).*100);

% Accuracy
accuracy_anger = ((tp_anger+tn_anger)./(tp_anger+fp_anger+fn_anger+tn_anger).*100);
accuracy_sad = ((tp_sad+tn_sad)./(tp_sad+fp_sad+fn_sad+tn_sad).*100);
accuracy_fear = ((tp_fear+tn_fear)./(tp_fear+fp_fear+fn_fear+tn_fear).*100);
accuracy_happy = ((tp_happy+tn_happy)./(tp_happy+fp_happy+fn_happy+tn_happy).*100);
accuracy_disgust = ((tp_disgust+tn_disgust)./(tp_disgust+fp_disgust+fn_disgust+tn_disgust).*100);
accuracy_neutral = ((tp_neutral+tn_neutral)./(tp_neutral+fp_neutral+fn_neutral+tn_neutral).*100);
avg_detect_rate = (accuracy_anger+accuracy_sad+accuracy_fear+accuracy_happy+accuracy_neutral+accuracy_disgust)/no_of_class;

% Senstivity/Recall
sens_anger = tp_anger./ (tp_anger+fn_anger)*100;
sens_sad = tp_sad./ (tp_sad+fn_sad)*100;
sens_disgust = tp_disgust./ (tp_disgust+fn_disgust)*100;
sens_neutral = tp_neutral./ (tp_neutral+fn_neutral)*100;
sens_fear = tp_fear./ (tp_fear+fn_fear)*100;
sens_happy = tp_happy./ (tp_happy+fn_happy)*100;
recall_rate = (sens_anger+sens_sad+sens_disgust+sens_fear+sens_neutral+sens_happy)/no_of_class;

% Precision
prec_anger = tp_anger./ (tp_anger+fp_anger)*100;
prec_sad = tp_sad./ (tp_sad+fp_sad)*100;
prec_disgust = tp_disgust./ (tp_disgust+fp_disgust)*100;
prec_neutral = tp_neutral./ (tp_neutral+fp_neutral)*100;
prec_fear = tp_fear./ (tp_fear+fp_fear)*100;
prec_happy = tp_happy./ (tp_happy+fp_happy)*100;
precision_rate = (prec_anger+prec_sad+prec_disgust+prec_fear+prec_neutral+prec_happy)/no_of_class;

% F1-score harmonic mean of precision and recall
f1_anger = 2*(prec_anger*sens_anger)./(prec_anger+sens_anger);
f1_sad = 2*(prec_sad*sens_sad)./(prec_sad+sens_sad);
f1_disgust = 2*(prec_disgust*sens_disgust)./(prec_disgust+sens_disgust);
f1_neutral = 2*(prec_neutral*sens_neutral)./(prec_neutral+sens_neutral);
f1_fear = 2*(prec_fear*sens_fear)./(prec_fear+sens_fear);
f1_happy = 2*(prec_happy*sens_happy)./(prec_happy+sens_happy);

% False Positive Rate
err_anger = fp_anger./ (tn_anger+fp_anger)*100;
err_sad = fp_sad./ (tn_sad+fp_sad)*100;
err_disgust = fp_disgust./ (tn_disgust+fp_disgust)*100;
err_neutral = fp_neutral./ (tn_neutral+fp_neutral)*100;
err_fear = fp_fear./ (tn_fear+fp_fear)*100;
err_happy = fp_happy./ (tn_happy+fp_happy)*100;
error_rate = (err_anger+err_sad+err_disgust+err_fear+err_neutral+err_happy)/no_of_class;

% Specificity
spec_anger = tn_anger./ (tn_anger+fp_anger)*100;
spec_sad = tn_sad./ (tn_sad+fp_sad)*100;
spec_disgust = tn_disgust./ (tn_disgust+fp_disgust)*100;
spec_neutral = tn_neutral./ (tn_neutral+fp_neutral)*100;
spec_fear = tn_fear./ (tn_fear+fp_fear)*100;
spec_happy = tn_happy./ (tn_happy+fp_happy)*100;
spec_rate = (spec_anger+spec_sad+spec_disgust+spec_fear+spec_neutral+spec_happy)/no_of_class;

% Balanced Accuracy (BAC), average of precision and recall
bac_anger = (sens_anger +spec_anger)/2;
bac_sad = (sens_sad +spec_sad)/2;
bac_disgust = (sens_disgust +spec_disgust)/2;
bac_happy = (sens_happy +spec_happy)/2;
bac_neutral = (sens_neutral +spec_neutral)/2;
bac_fear = (sens_fear +spec_fear)/2;
bac= (bac_anger+bac_sad+bac_disgust+bac_fear+bac_neutral+bac_happy)/no_of_class;

% Correct classification rate
perc_corr_classemotion = (tp_anger+tp_sad+tp_fear+tp_happy+tp_disgust+tp_neutral)./(sum(sum(ed_conf_matrix))).*100;

Thresh_accu(1,count) = cus_threshold; % absolute confidence threshold
Thresh_accu(2,count) = unclas_rate_emotion; % rejection rate
Thresh_accu(3,count) = perc_corr_classemotion; % correct classification rate

end
% DL recall for each individual emotion - anger, sad, disgust, neutral, happy, fear
recall_emotion = [sens_anger, sens_sad, sens_disgust, sens_neutral, sens_happy, sens_fear];

num_emotion = zeros(1, no_of_class);
% Count the number of instances for each emotion (anger, sad, disgust, neutral, happy, fear)
num_emotion(1) = length(find(sgroup==3));
num_emotion(2) = length(find(sgroup==4));
num_emotion(3) = length(find(sgroup==1));
num_emotion(4) = length(find(sgroup==5));
num_emotion(5) = length(find(sgroup==2));
num_emotion(6) = length(find(sgroup==6));

end
