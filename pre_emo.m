function [output] = pre_emo(pathname, filename)

prefix = '../Excels/Feature';
xlsname_raw_notnorm = [prefix,'_raw_notnorm.xls'];
[training_num, training_text, training_all] = xlsread(xlsname_raw_notnorm);

%[train_coeff, train_score] = pca(training_num);
training_text = training_text(2:end, 1);
training_labels = cell(size(training_num, 1), 1);

for ii = 1:length(training_text)
    b = training_labels(ii);
    a = b(1);
    training_labels(ii) = cell2mat(training_text(ii));
    for iii = 1:size(training_labels(ii), 1)
        a(iii)
        if strcmp(a(iii), '_')
            break;
        end
    end
    training_labels(ii) = a(1:iii);
end

%[input, fs] = wavread([pathname filename]);
features = featext(pathname, filename);

training_groups = ['a'; 'a'; 'a'; 'a'; 'a';'a'; 'a'; 'a';'a'; 'a'; 'h'; 'h'; 'h'; 'h'; 'h'; 'h'; 'h';'h'; 'h'; 'h'; 'n'; 'n'; 'n'; 'n'; 'n'; 'n'; 'n'; 'n'; 'n'; 'n'; 'n'; 'p'; 'p'; 'p';'p'; 'p'; 'p'; 'p'; 'p'; 'p'; 'p'; 's'; 's'; 's'; 's';'s';'s'; 's'; 's'; 's'; 's'; 'r';'r'; 'r'; 'r';'r';'r'; 'r'; 'r'; 'r'; 'r'];
Mdl = ClassificationKNN.fit(training_num, training_labels);%ClassificationKNN.fit(training_num, training_text);
output = predict(Mdl, features);
disp(output);
end
    