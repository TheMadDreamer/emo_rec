function [svmmodel] = simple_svm(pathname, filename, testname)
    [num, txt, all] = xlsread([pathname filename]);
    %normalisation
    minimums = min(num, [], 1);
    ranges = max(num, [], 1) - minimums;
    num = (num - repmat(minimums, size(num, 1), 1)) ./ repmat(ranges, size(num, 1), 1);
    %disp(c);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    namefeats = txt(1, 1:end); % name of all the features in cell
    names = txt(2:end, 1);      % contains the names of all the files from which any small sample belongs to
    namepts = ones(1,1);
    labels = txt(2:end, 2);     %stores all the emotion labels
    %labeled = ones(1, 6);       %indices where labels change
    i = 2;
    j = 1;
    %prevname = cell2mat(names(1));
    prev = cell2mat(labels(1));
    
    % assuming at least 2 files**
    labelss = ones(length(labels), 1);
    while (i <= length(labels))
        %currentname = cell2mat(names(i));
        current = cell2mat(labels(i));
        if strcmp(current, prev) ~= 1
            j = j + 1;
        end
        labelss(i, 1) = j;
        %if strcmp(currentname, prevname) ~= 1
         %   namepts = vertcat(namepts, i-1);
        %end
        prev = current;
        %prevname = currentname;
        i = i + 1;
    end
    
    labelst = labelss;
    numt = num;
    if (nargin >=3)
        [numt, txtt, allt] = xlsread([pathname testname]);
        %normalisation
        minimums = min(numt, [], 1);
        ranges = max(numt, [], 1) - minimums;
        numt = (numt - repmat(minimums, size(numt, 1), 1)) ./ repmat(ranges, size(numt, 1), 1);
        %disp(c);
        
        labelst = txtt(2:end, 2);
        % assuming at least 2 files**
        labelsst = ones(length(labelst), 1);
        prev = cell2mat(labelst(1,1));
        i = 2;
        j = 1;
        while (i < length(labelst) + 1)
            %currentname = cell2mat(names(i));
            current = cell2mat(labelst(i));
            if strcmp(current, prev) ~= 1
                j = j + 1;
            end
            labelsst(i, 1) = j;
            prev = current;
            i = i + 1;
        end
    end
    
    svmmodel = svmtrain(labelss, num, '-t 1 -d 3 -c 100');
    %disp(labelsst);
    s = svmpredict(labelsst, numt, svmmodel);
    disp(s);
end