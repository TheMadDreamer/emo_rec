function [avg_rej_rate, avg_cor_rate, E] = interp(a, num)
% % Linear interpolation to calculate the average correct classification
% rate over num trials/speakers/folds
%
% Input arguments:
% a -> a (2*num) * length(thresh) matrix with (2*j-1)th row being the rejection rate,
% (2*j)th row being the correct classification rate (j = 1:num)
% num -> number of trials/speakers/folds to average over

% Output:
% avg_rej_rate -> the interpolated average rejection rate
% avg_cor_rate -> the average correct classification rate over num

% remove NaNs in the end
[r, c] = find(isnan(a));
if ~isempty(r)
    col_id = min(c);
    a = a(:,1:col_id-1);
end
    
% Calculate the last rejection rate used for interpolation
step = 2;
rej = zeros(num,1);
for cnt = 1:num
    rej(cnt) = a(2*cnt-1,end);
end
last = floor(min(rej));
upper = last - mod(last,step) - step;
for i = 0:step:upper
    for j = 1:num
        idx = find(a(2*j-1,:) > i);
        pos = min(idx);
        y(j,i/step+1) = (a(2*j,pos) - a(2*j,pos-1))*(i-a(2*j-1,pos-1))/(a(2*j-1,pos) - a(2*j-1,pos-1)) + a(2*j,pos-1);    
    end
end
avg_rej_rate = 0:step:upper;
avg_cor_rate = mean(y(1:j,:));
E = std(y); % used to plot the error bar