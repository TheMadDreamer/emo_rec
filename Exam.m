%function [ output_args ] = Exam( input_args )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
X1 = ones(10,2);
X1(1:10,1) = 2;
for ii = 1:10
    X1(ii, 2) = 0.5*sin(20*ii);
end

X2 = ones(10,2);
X2(1:10,1) = 4;
for ii = 1:10
    X2(ii, 2) = 0.5*sin(20*ii);
end

X3 = ones(10,2);
X3(1:10,1) = 6;
for ii = 1:10
    X3(ii, 2) = 0.5*sin(20*ii);
end

X = [X1;X2;X3];
%end

K = ones(30,30);
for ii = 1:30
    for jj = 1:30
        a = norm(X(ii,:) - X(jj,:));
        K(ii, jj) = exp(-3*a*a);
    end
end

for ii = 1:10
    for jj = 1:10
        model = svmtrain(Y, X, '-g' ii '-c' jj/10');
        svmpredict(Y, X, model);
    end
end