%histogram plot
%pathname and filename for the file to train the model
%k : Number of clusters to be formed during classification using k means
%skr : skip ratio to select the specified number of samples for training
%m: == 0 if NaNs are to be masked with zeroes and 1 to mask NaN with the
    %mean of that row
%testname : name of the test case file
function [svmodel, s] = histagram(pathname, filename, k, skr, m, testname)
    [idx, c, labeled, namepts] = kcents(pathname, filename, k, skr, m);
    tabulatew = tabulate(idx);
     %%%%%%% mask NaN with 0 %%%%%%%%%%%
    if (m == 0)
    mask = isnan(c);
    c(mask) = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% mask NaN with mean %%%%%%%%%%
    if (m ~= 0)
    maskmean = nanmean(c);
    for ii = 1:size(c, 2)
        maskk = isnan(c(:, ii));
        c(maskk, ii) = maskmean(ii);
    end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %disp(tabulatew);
    %groupes = 1:k;
    %s = sprintf('k is %d', k);
    %ct = zscore(c);
    %disp(c);
    %normalisation
    %minimums = min(c, [], 1);
    %ranges = max(c, [], 1) - minimums;
    %c = (c - repmat(minimums, size(c, 1), 1)) ./ repmat(ranges, size(c, 1), 1);
    %disp(c);
    %test_data = (test_data - repmat(minimums, size(test_data, 1), 1)) ./ repmat(ranges, size(test_data, 1), 1);
    %svmstruct = svmtrain(c, groupes');
    %pre = svmpredict(groupes', c, svmstruct);
    [num, txt] = xlsread([pathname filename]);
     %%%%%%% mask NaN with 0 %%%%%%%%%%%
    if (m == 0)
    mask = isnan(num);
    num(mask) = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% mask NaN with mean %%%%%%%%%%
    if (m ~= 0)
    maskmean = nanmean(num);
    for ii = 1:size(num, 2)
        maskk = isnan(num(:, ii));
        num(maskk, ii) = maskmean(ii);
    end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    his = ones(length(namepts), k);
    groups = ones(length(namepts)-1, 1);
    count_for_labels = 1;
    for i = 2:length(namepts)
        %predict the classes (for frequency) for each file
        if (i ~= length(namepts))
            classes = knpredict(c, num(namepts(i, 1):(namepts(i+1, 1) - 1), 1:end));
            %classes = svmpredict(ones(-namepts(i, 1)+namepts(i+1, 1), 1), num(namepts(i, 1):(namepts(i+1, 1) - 1), 1:end), svmstruct);
            %disp(classes);
        else
            classes = knpredict(c, num(namepts(i, 1):size(num, 1), 1:end));
            %classes = svmpredict(ones(size(num, 1) - namepts(i)+1, 1), num(namepts(i):size(num, 1), 1:end), svmstruct);
        end
        %make histogram for all files
        for ll = 1:length(classes)
            if (classes(ll) <= k)
                his(i, classes(ll)) = his(i, classes(ll)) + 1;
            end
        end

        if (count_for_labels < 6)
                if (namepts(i) == labeled(count_for_labels + 1))
                    count_for_labels = count_for_labels + 1;
                end
        end
        groups(i) = count_for_labels;
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Test %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (nargin >= 6)
        
        [idxt, ct, labeledt, nameptst] = kcents(pathname, testname, k, skr, m);
        [numt, txtt] = xlsread([pathname testname]);
    %minimumst = min(ct, [], 1);
    %rangest = max(ct, [], 1) - minimumst;
    %ct = (ct - repmat(minimumst, size(ct, 1), 1)) ./ repmat(rangest, size(ct, 1), 1);
    %%%%%%% mask NaN with 0 %%%%%%%%%%%
    if (m == 0)
    maskt = isnan(numt);
    numt(maskt) = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%% mask NaN with mean %%%%%%%%%%
    if (m ~= 0)
    maskmeant = nanmean(numt);
    for ii = 1:size(numt, 2)
        maskkt = isnan(numt(:, ii));
        numt(maskkt, ii) = maskmeant(ii);
    end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    histo = ones(length(nameptst), k);
    groupst = ones(length(nameptst)-1, 1);
    count_for_labelst = 1;
    for i = 2:length(nameptst)
        %predict the classes (for frequency) for each file
        if (i ~= length(nameptst))
            classest = knpredict(c, numt(nameptst(i, 1):(nameptst(i+1, 1) - 1), 1:end));
            %classes = svmpredict(ones(-namepts(i, 1)+namepts(i+1, 1), 1), num(namepts(i, 1):(namepts(i+1, 1) - 1), 1:end), svmstruct);
            %disp(classes);
        else
            classest = knpredict(c, numt(nameptst(i, 1):size(numt, 1), 1:end));
            %classes = svmpredict(ones(size(num, 1) - namepts(i)+1, 1), num(namepts(i):size(num, 1), 1:end), svmstruct);
        end
        %make histogram for all files
        for ll = 1:length(classest)
            if (classest(ll) <= k)
                histo(i, classest(ll)) = histo(i, classest(ll)) + 1;
            end
        end

        if (count_for_labelst < 6)
                if (nameptst(i) == labeledt(count_for_labelst + 1))
                    count_for_labelst = count_for_labelst + 1;
                end
        end
        groupst(i) = count_for_labelst;
    end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %disp(his);
    minimums = min(his, [], 1);
    ranges = max(his, [], 1) - minimums;
    his = (his - repmat(minimums, size(his, 1), 1)) ./ repmat(ranges, size(his, 1), 1);
    svmodel = svmtrain(groups, his, '-t 1 -d 3 -c 100000');
    %disp(groups);
    ss = svmpredict(groups, his, svmodel);
    %disp(ss);
    if (nargin >=6)
        %disp(histo);
        %disp(groupst);
        minimums = min(histo, [], 1);
        ranges = max(histo, [], 1) - minimums;
        histo = (histo - repmat(minimums, size(histo, 1), 1)) ./ repmat(ranges, size(histo, 1), 1);
        s = svmpredict(groupst, histo, svmodel);
        disp(s);
    end
end

% {1 = 'anger'; 2 = 'happy'; 3 = 'neutral'; 4 = 'panic', 5 = 'sad', 6 =
% 'sur'};

%filesi = {1};
    % Convert from cell to char array
    %names = txt(2:end, 1);
    %prev = cell2mat(names(1));
    %i = 2; j = 1;
    %while (i < length(names))
    %    current = cell2mat(names(i));
    %    if strcmp(prev, current) ~= 1
    %        filesi = vertcat(filesi, i);
    %        j = j + 1;
    %    end
    %    prev = current;
    %   i = i + 1;
    %end
    %%%%%%
    