function  LOSO_generate(datasheet, train_savepath, test_savepath)
% Generate Dataset for leave-one-subject-out (LOSO) test;
% Six emotions for 4 female and 3 male speakers;
% Input Aguement: 
% - datasheet: the full dataset containing all samples; default: datasheet='../Excels/Feature_LDC_Clean_norm.xls'
% - train_savepath: path for storing training data; default: train_savepath='../Datasets/Dataset_LDC_Clean_norm/LOSO_train/'
% - test_savepath: path for storing testing data; default: test_savepath='../Datasets/Dataset_LDC_Clean_norm/LOSO_test/'
%
% See the Bridge project website at Wireless Communication and Networking
% Group (WCNG), University of Rochester for more information: 
% http://www.ece.rochester.edu/projects/wcng/project_bridge.html
%
% Written by Jianbo Yuan, University of Rochester.
% Copyright (c) 2013 University of Rochester.
% Version: October 2013.
%
% Permission to use, copy, modify, and distribute this software without 
% fee is hereby granted FOR RESEARCH PURPOSES only, provided that this
% copyright notice appears in all copies and in all supporting 
% documentation, and that the software is not redistributed for any fee.
%
% For any other uses of this software, in original or modified form, 
% including but not limited to consulting, production or distribution
% in whole or in part, specific prior permission must be obtained from WCNG.
% Algorithms implemented by this software may be claimed by patents owned 
% by WCNG, University of Rochester.
%
% The WCNG makes no representations about the suitability of this 
% software for any purpose. It is provided "as is" without express
% or implied warranty.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialization;
emotion = {'disgust','happy','hotAnger','sadness','neutral','panic'};
sp = {'cc', 'cl', 'gg', 'jg', 'mf', 'mk', 'mm'}; % speakers' initials on filenames
num_sp = length(sp);
num_em = length(emotion);

% make path for training and testing data;
mkdir(train_savepath);
mkdir(test_savepath);
save_train = train_savepath;
save_test = test_savepath;

%% Read data from datasheet
file = datasheet;
[data, strings] = xlsread(file);
% Header for all statistical features
header = strings(1,:); 
% File names staring with speaker initials, first column
colum = strings(2:end,1);

for cnt_sp = 1:num_sp
    % For each speaker, find his/her samples
    me = find(strncmp(colum(:),sp{cnt_sp},2));
    % Find samples for other speakers
    other = find(~strncmp(colum(:),sp{cnt_sp},2));
    train_title = colum(other);
    test_title = colum(me);
    train_data = data(other,:);
    test_data = data(me,:);
    % Store loso data
    dir_test = save_test;
    dir_train = save_train;    
    % Restore data and headers for testing data
    test_xls = [dir_test 'LOSO_test_' sp{cnt_sp} '.xls'];
    xlswrite(test_xls, header);
    xlswrite(test_xls, test_title, 'Sheet1', 'A2');
    xlswrite(test_xls, test_data, 'Sheet1', 'B2'); 
    % Restore training data, with different emotions
    % Same samples with labeling changed into 1 for positive and 2 for negative
    for cnt_em = 1:num_em
        [m,n] = size(train_data);
        label = zeros(m,1);      
        label(train_data(:,1) == cnt_em) = 1;
        label(train_data(:,1) ~= cnt_em) = 2;
        save_data = zeros(m,n);
        save_data(:,1) = label;
        save_data(:,2:end) = train_data(:,2:end);
        
        save_train_xls = [dir_train emotion{cnt_em} '_AgainstAll_LOSO_train_' sp{cnt_sp} '.xls'];
        xlswrite(save_train_xls, header);
        xlswrite(save_train_xls, train_title, 'Sheet1', 'A2');
        xlswrite(save_train_xls, save_data, 'Sheet1', 'B2');             
    end

disp(sp{cnt_sp});
end
