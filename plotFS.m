clear;
close all;
clc;

% % Plot rejection rate vs. correct classification rate with different number
% of features selected
% Input:
% xlsname -> excel file containing the results for different number
% of features selected, i.e. top 20,40,60,80,100,121 features in six sheets.

xlsname = '../Excels/raw_OAA_result.xls';

featureSet = [20,40,60,80,100,121];
num_trial = 5;

fig1 = figure;
hold on;

for m = 1:6
    % read data from each spreadsheet
    sheet = ['sheet' num2str(m)];
    tmp = xlsread(xlsname, sheet);
    % construct a (2*num_trial) * cnt_thresh matrix with (2*j-1)th row being the rejection rate,
    % (2*j)th row being the correct classification rate (j = 1:num_trial)
    num = zeros(2*num_trial, size(tmp,2));
    for cnt = 1:num_trial
        num(2*cnt-1,:) = tmp(3*cnt-1,:);
        num(2*cnt,:) = tmp(3*cnt,:);
    end
    % calculate the average correct classification rate over num_trial upsampling trials
    [avg_rej_rate, avg_cor_rate, E] = interp(num, num_trial);
    switch m
        case 1            
            errorbar(avg_rej_rate, avg_cor_rate, E, '-*g','LineWidth',2);
        case 2         
            errorbar(avg_rej_rate, avg_cor_rate, E, '-*r','LineWidth',2);
        case 3
            errorbar(avg_rej_rate, avg_cor_rate, E, '-*b','LineWidth',2);
        case 4
            errorbar(avg_rej_rate, avg_cor_rate, E, '-*k','LineWidth',2);
        case 5
            errorbar(avg_rej_rate, avg_cor_rate, E, '-*c','LineWidth',2);
        case 6
            errorbar(avg_rej_rate, avg_cor_rate, E, '-*y','LineWidth',2);
    end
end

h1 = xlabel('Rejection rate (%)'); 
h2 = ylabel('Decision-level correct classification rate (%)');
xlim([0 80]);
ylim([0 100]);
legend('20','40','60','80','100','121','Location','SouthEast');
grid on;