function [V,W] = valloc(y)
% Written by Weiyang Cai, University of Rochester.
% V is the amplitude of peak, W1 shows the index of the peak
L = length(y);
V = ones(1,L);
for i = 2:(L-1)
    if y(i)<y(i-1) && y(i)< y(i+1)
        V(i) = y(i);
    end
end
W = find(V~=1);