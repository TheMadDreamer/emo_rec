% Classifier level OAA classification for each emotion (Disgust, Happy,
% Anger, Sad, Neutral and Fear) 
%
% See the Bridge project website at Wireless Communication and Networking
% Group (WCNG), University of Rochester for more information: 
% http://www.ece.rochester.edu/projects/wcng/project_bridge.html
%
% Written by Jianbo Yuan, Yun Zhou University of Rochester.
% Copyright (c) 2014 University of Rochester.
% Version: June 2014.
%
% Permission to use, copy, modify, and distribute this software without 
% fee is hereby granted FOR RESEARCH PURPOSES only, provided that this
% copyright notice appears in all copies and in all supporting 
% documentation, and that the software is not redistributed for any fee.
%
% For any other uses of this software, in original or modified form, 
% including but not limited to consulting, production or distribution
% in whole or in part, specific prior permission must be obtained from WCNG.
% Algorithms implemented by this software may be claimed by patents owned 
% by WCNG, University of Rochester.
%
% The WCNG makes no representations about the suitability of this 
% software for any purpose. It is provided "as is" without express
% or implied warranty.
%

clear all;
close all;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Parameter settings
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
no_of_class = 6; % number of emotion classes
num_folds = 7; % number of rounds of cross-validations
feature = 4:124; % select feature column indices in Excel sheets
dataset = 'uneven'; % Select dataset (even or uneven) 
dirData = '../Datasets/Dataset_LDC_Clean_norm_backup'; % folder for partitioned data
trainfilepath = [dirData '/train_' dataset]; % path where the training data is located
testfilepath = [dirData '/test']; % path where the testing data is located
m = 5; % upsample 5 times
num_f = length(feature);

% Kernel settings: rbf
kernel_selection = 'rbf';

% Optimization Method - SMO
options = svmsmoset('maxiter',500000);

% record precision, recall, f1 score, FPR, accuracy and calculate avg/std
anger = zeros(5,9);
sad = zeros(5,9);
happy = zeros(5,9);
disgust = zeros(5,9);
fear = zeros(5,9);
neutral = zeros(5,9);

for cnt = 1:num_folds
    
    disp(['Running cross validation fold ' num2str(cnt) '...']);
    disp('----------------------------------');
    % TRAINING
    % HAPPY EMOTION
    numbers_happy = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_happy_AgainstAll_' dataset '.xls']));
    % SAD EMOTION
    numbers_sad = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_sadness_AgainstAll_' dataset '.xls']));
    % FEAR EMOTION
    numbers_fear = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_panic_AgainstAll_' dataset '.xls']));
    % ANGER EMOTION
    numbers_anger = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_hotAnger_AgainstAll_' dataset '.xls']));
    % DISGUST EMOTION
    numbers_disgust = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_disgust_AgainstAll_' dataset '.xls']));
    % NEUTRAL EMOTION
    numbers_neutral = xlsread(fullfile(trainfilepath,['train_' num2str(cnt) '_neutral_AgainstAll_' dataset '.xls']));

    % TEST
    numbers = xlsread(fullfile(testfilepath, ['test_' num2str(cnt) '.xls']));

    % The features are read into matrix
    sdata_happy = numbers_happy(:,feature);
    % Grouped based on Individual Emotion - Happy
    sgroup_happy = numbers_happy(:,1);

    % The features are read into matrix
    sdata_sad = numbers_sad(:,feature);
    % Grouped based on Individual Emotion - Sad
    sgroup_sad = numbers_sad(:,1);

    % The features are read into matrix
    sdata_fear = numbers_fear(:,feature);
    % Grouped based on Individual Emotion -Fear
    sgroup_fear = numbers_fear(:,1); 

    % The features are read into matrix
    sdata_anger = numbers_anger(:,feature);
    % Grouped based on Individual Emotion - Anger
    sgroup_anger = numbers_anger(:,1);

    % The features are read into matrix
    sdata_disgust = numbers_disgust(:,feature);
    % Grouped based on Individual Emotion - Disgust
    sgroup_disgust = numbers_disgust(:,1);

    % The features are read into matrix
    sdata_neutral = numbers_neutral(:,feature);
    % Grouped based on Individual Emotion - Neutral
    sgroup_neutral = numbers_neutral(:,1);

    %% upsampling
    [sdata_neutral, sgroup_neutral] = multi(sdata_neutral, sgroup_neutral,m,num_f);
    [sdata_disgust, sgroup_disgust] = multi(sdata_disgust, sgroup_disgust,m,num_f);
    [sdata_anger, sgroup_anger] = multi(sdata_anger, sgroup_anger,m,num_f);
    [sdata_fear, sgroup_fear] = multi(sdata_fear, sgroup_fear,m,num_f);
    [sdata_sad, sgroup_sad] = multi(sdata_sad, sgroup_sad,m,num_f);
    [sdata_happy, sgroup_happy] = multi(sdata_happy, sgroup_happy,m,num_f);

    sgroup = numbers(:,1);
    testdata = numbers(:,feature);

    %% Train each individual One-Against-All classifiers
    svmStructs_happy = svmtrain(sdata_happy,sgroup_happy,'Kernel_Function',kernel_selection,...
        'Method','SMO','rbf_sigma',5,'smo_opts',options,'autoscale',true);
    svmStructs_sad = svmtrain(sdata_sad,sgroup_sad,'Kernel_Function',kernel_selection,...
        'Method','SMO','rbf_sigma',5,'smo_opts',options,'autoscale',true);
    svmStructs_fear = svmtrain(sdata_fear,sgroup_fear,'Kernel_Function',kernel_selection,...
        'Method','SMO','rbf_sigma',5,'smo_opts',options,'autoscale',true);
    svmStructs_anger = svmtrain(sdata_anger,sgroup_anger,'Kernel_Function',kernel_selection,...
        'Method','SMO','rbf_sigma',5,'smo_opts',options,'autoscale',true);
    svmStructs_disgust = svmtrain(sdata_disgust,sgroup_disgust,'Kernel_Function',kernel_selection,...
        'Method','SMO','rbf_sigma',5,'smo_opts',options,'autoscale',true);
    svmStructs_neutral = svmtrain(sdata_neutral,sgroup_neutral,'Kernel_Function',kernel_selection,...
        'Method','SMO','rbf_sigma',5,'smo_opts',options,'autoscale',true);

    % Use the model trained for each SVM classifiers to predict the label and confidence value 
    %  for the test instances
    [classes_happy, confident_happy] = svmclassify(svmStructs_happy,testdata);
    [classes_sad, confident_sad] = svmclassify(svmStructs_sad,testdata);
    [classes_fear, confident_fear] = svmclassify(svmStructs_fear,testdata);
    [classes_anger, confident_anger] = svmclassify(svmStructs_anger,testdata);
    [classes_disgust, confident_disgust] = svmclassify(svmStructs_disgust,testdata);
    [classes_neutral, confident_neutral] = svmclassify(svmStructs_neutral,testdata);

    % % According to the database: Unseen data is grouped as
    % % 1 - Disgust,
    % % 2 - Happy,
    % % 3 - Angry
    % % 4 -  Sad
    % % 5 - Neutral
    % % 6 - Fear/Panic

    tp_disgust=length(find(classes_disgust(find(sgroup==1))==1));
    fn_disgust=length(find(classes_disgust(find(sgroup==1))==2));
    tn_disgust=length(find(classes_disgust(find(sgroup~=1))==2));
    fp_disgust=length(find(classes_disgust(find(sgroup~=1))==1));

    tp_happy=length(find(classes_happy(find(sgroup==2))==1));
    fn_happy=length(find(classes_happy(find(sgroup==2))==2));
    tn_happy=length(find(classes_happy(find(sgroup~=2))==2));
    fp_happy=length(find(classes_happy(find(sgroup~=2))==1));

    tp_anger=length(find(classes_anger(find(sgroup==3))==1));
    fn_anger=length(find(classes_anger(find(sgroup==3))==2));
    tn_anger=length(find(classes_anger(find(sgroup~=3))==2));
    fp_anger=length(find(classes_anger(find(sgroup~=3))==1));

    tp_sad=length(find(classes_sad(find(sgroup==4))==1));
    fn_sad=length(find(classes_sad(find(sgroup==4))==2));
    tn_sad=length(find(classes_sad(find(sgroup~=4))==2));
    fp_sad=length(find(classes_sad(find(sgroup~=4))==1));

    tp_neutral=length(find(classes_neutral(find(sgroup==5))==1));
    fn_neutral=length(find(classes_neutral(find(sgroup==5))==2));
    tn_neutral=length(find(classes_neutral(find(sgroup~=5))==2));
    fp_neutral=length(find(classes_neutral(find(sgroup~=5))==1));

    tp_fear=length(find(classes_fear(find(sgroup==6))==1));
    fn_fear=length(find(classes_fear(find(sgroup==6))==2));
    tn_fear=length(find(classes_fear(find(sgroup~=6))==2));
    fp_fear=length(find(classes_fear(find(sgroup~=6))==1));

    %% Performance Measures
    % SVM performance based on accuracy, sensitivity (recall), precision,
    % error rate and balanced accuracy (BAC)
    accuracy_anger = ((tp_anger+tn_anger)./(tp_anger+fp_anger+fn_anger+tn_anger).*100);
    accuracy_sad = ((tp_sad+tn_sad)./(tp_sad+fp_sad+fn_sad+tn_sad).*100);
    accuracy_fear = ((tp_fear+tn_fear)./(tp_fear+fp_fear+fn_fear+tn_fear).*100);
    accuracy_happy = ((tp_happy+tn_happy)./(tp_happy+fp_happy+fn_happy+tn_happy).*100);
    accuracy_disgust = ((tp_disgust+tn_disgust)./(tp_disgust+fp_disgust+fn_disgust+tn_disgust).*100);
    accuracy_neutral = ((tp_neutral+tn_neutral)./(tp_neutral+fp_neutral+fn_neutral+tn_neutral).*100);
    avg_detect_rate = (accuracy_anger+accuracy_sad+accuracy_disgust+accuracy_fear+accuracy_neutral+accuracy_happy)/no_of_class;

    sens_anger = tp_anger./ (tp_anger+fn_anger)*100;
    sens_sad = tp_sad./ (tp_sad+fn_sad)*100;
    sens_disgust = tp_disgust./ (tp_disgust+fn_disgust)*100;
    sens_neutral = tp_neutral./ (tp_neutral+fn_neutral)*100;
    sens_fear = tp_fear./ (tp_fear+fn_fear)*100;
    sens_happy = tp_happy./ (tp_happy+fn_happy)*100;
    recall_rate = (sens_anger+sens_sad+sens_disgust+sens_fear+sens_neutral+sens_happy)/no_of_class;

    prec_anger = tp_anger./ (tp_anger+fp_anger)*100;
    prec_sad = tp_sad./ (tp_sad+fp_sad)*100;
    prec_disgust = tp_disgust./ (tp_disgust+fp_disgust)*100;
    prec_neutral = tp_neutral./ (tp_neutral+fp_neutral)*100;
    prec_fear = tp_fear./ (tp_fear+fp_fear)*100;
    prec_happy = tp_happy./ (tp_happy+fp_happy)*100;
    precision_rate = (prec_anger+prec_sad+prec_disgust+prec_fear+prec_neutral+prec_happy)/no_of_class;

    f1_anger = 2*(prec_anger*sens_anger)./(prec_anger+sens_anger);
    f1_sad = 2*(prec_sad*sens_sad)./(prec_sad+sens_sad);
    f1_disgust = 2*(prec_disgust*sens_disgust)./(prec_disgust+sens_disgust);
    f1_neutral = 2*(prec_neutral*sens_neutral)./(prec_neutral+sens_neutral);
    f1_fear = 2*(prec_fear*sens_fear)./(prec_fear+sens_fear);
    f1_happy = 2*(prec_happy*sens_happy)./(prec_happy+sens_happy);

    err_anger = fp_anger./ (fp_anger+tn_anger)*100;
    err_sad = fp_sad./ (fp_sad+tn_sad)*100;
    err_disgust = fp_disgust./ (fp_disgust+tn_disgust)*100;
    err_neutral = fp_neutral./ (fp_neutral+tn_neutral)*100;
    err_fear = fp_fear./ (fp_fear+tn_fear)*100;
    err_happy = fp_happy./ (fp_happy+tn_happy)*100;
    error_rate = (err_anger+err_sad+err_disgust+err_fear+err_neutral+err_happy)/no_of_class;

    spec_anger = tn_anger./ (tn_anger+fp_anger)*100;
    spec_sad = tn_sad./ (tn_sad+fp_sad)*100;
    spec_disgust = tn_disgust./ (tn_disgust+fp_disgust)*100;
    spec_neutral = tn_neutral./ (tn_neutral+fp_neutral)*100;
    spec_fear = tn_fear./ (tn_fear+fp_fear)*100;
    spec_happy = tn_happy./ (tn_happy+fp_happy)*100;
    spec_rate = (spec_anger+spec_sad+spec_disgust+spec_fear+spec_neutral+spec_happy)/no_of_class;

    bac_anger = (sens_anger +spec_anger)/2;
    bac_sad = (sens_sad +spec_sad)/2;
    bac_disgust = (sens_disgust +spec_disgust)/2;
    bac_happy = (sens_happy +spec_happy)/2;
    bac_neutral = (sens_neutral +spec_neutral)/2;
    bac_fear = (sens_fear +spec_fear)/2;
    bac= (bac_anger+bac_sad+bac_disgust+bac_fear+bac_neutral+bac_happy)/no_of_class;

    anger(1,cnt) = prec_anger;
    anger(2,cnt) = sens_anger;
    anger(3,cnt) = f1_anger;
    anger(4,cnt) = err_anger;
    anger(5,cnt) = accuracy_anger;

    sad(1,cnt) = prec_sad;
    sad(2,cnt) = sens_sad;
    sad(3,cnt) = f1_sad;
    sad(4,cnt) = err_sad;
    sad(5,cnt) = accuracy_sad;

    happy(1,cnt) = prec_happy;
    happy(2,cnt) = sens_happy;
    happy(3,cnt) = f1_happy;
    happy(4,cnt) = err_happy;
    happy(5,cnt) = accuracy_happy;

    disgust(1,cnt) = prec_disgust;
    disgust(2,cnt) = sens_disgust;
    disgust(3,cnt) = f1_disgust;
    disgust(4,cnt) = err_disgust;
    disgust(5,cnt) = accuracy_disgust;

    fear(1,cnt) = prec_fear;
    fear(2,cnt) = sens_fear;
    fear(3,cnt) = f1_fear;
    fear(4,cnt) = err_fear;
    fear(5,cnt) = accuracy_fear;

    neutral(1,cnt) = prec_neutral;
    neutral(2,cnt) = sens_neutral;
    neutral(3,cnt) = f1_neutral;
    neutral(4,cnt) = err_neutral;
    neutral(5,cnt) = accuracy_neutral;
end

for k = 1:5
    anger(k,8) = mean(anger(k,1:num_folds));
    anger(k,9) = std(anger(k,1:num_folds));
    sad(k,8) = mean(sad(k,1:num_folds));
    sad(k,9) = std(sad(k,1:num_folds));
    happy(k,8) = mean(happy(k,1:num_folds));
    happy(k,9) = std(happy(k,1:num_folds));
    disgust(k,8) = mean(disgust(k,1:num_folds));
    disgust(k,9) = std(disgust(k,1:num_folds));
    fear(k,8) = mean(fear(k,1:num_folds));
    fear(k,9) = std(fear(k,1:num_folds));
    neutral(k,8) = mean(neutral(k,1:num_folds));
    neutral(k,9) = std(neutral(k,1:num_folds));
end

% Display average accuracy for each emotion over k folds
result = [anger(5,8),sad(5,8),disgust(5,8),neutral(5,8),happy(5,8),fear(5,8)];
disp('**************************************************************');
disp('Accuracy for each emotion:');
fprintf('\tanger\t\tsad\t\tdisgust\tneutral\t\thappy\tfear\n');
disp(result);
disp('**************************************************************');

