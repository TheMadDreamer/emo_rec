function [y,fs,I,NumofSyllable,rate,Duration,Location] = speechrate(input,method, inputf, inputfs)
% output:
% y -> the samples of thhe speech signal.
% fs -> the sampling frequency.
% I -> the energy envelope of the speech signal
% NumofSyllable -> the total number of syllables in the speech signal.
% rate -> the speechrate of the speech signal.
% Duration -> the time duration of the speech signal.
% Location -> 1 at the peaks of the energy envelope, 0 otherwise

% input arguments
% input -> thename of the wav file you want to analyze.
% method can be 'moreband', 'lessband' or 'hybrid', which indicates the
% speaking rate estimation method.

% Written by Weiyang Cai, University of Rochester.
% Permission to use, copy, modify, and distribute this software without
% fee is hereby granted FOR RESEARCH PURPOSES only, provided that this
% copyright notice appears in all copies and in all supporting
% documentation, and that the software is not redistributed for any fee.
%
% For any other uses of this software, in original or modified form,
% including but not limited to consulting, production or distribution
% in whole or in part, specific prior permission must be obtained from WCNG.
% Algorithms implemented by this software may be claimed by patents owned
% by WCNG, University of Rochester.
%
% The WCNG makes no representations about the suitability of this
% software for any purpose. It is provided "as is" without express
% or implied warranty.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Default input parameters
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%parameter settings
thrd1= 29;
thrd2 = 13;


if (nargin == 2)
    [y,fs] = audioread(input);
end
if (nargin > 2)
    y = inputf;
    fs = inputfs;
end

% If the audio file has two channels, choose the channel with
    % better signal quality (higher total energy)
    if size(y,2) == 2
        total = zeros(1,2);
        for ch = 1:2
            temp = 0;
            for myi = 1:size(y,1)
                temp = temp + y(myi,ch)^2;
            end
            total(ch) = temp;
        end
        [~, channel] = max(total);
        y = y(:,channel);
    end

win = hamming(floor(0.06*fs));
overlap = floor(0.05*fs);
Duration = length(y)/fs;
NumofSyllable =0;
if(strcmp(method,'moreband'))
    I3 = crossenergy_moreband(y,win,overlap,fs);
end
if(strcmp(method,'hybrid'))
    I3 = hybridenergy(y, win,overlap,fs);
end
if(strcmp(method,'lessband'))
    I3 = crossenergy(y,win,overlap,fs);
end
I = I3/max(I3); %normalized amplitude
I = 20*log10(I); % transform to dB scale
[P,W1] =  peakloc(I); % P is the amplitude of peak, W1 shows the index of the peak
[V,W2] = valloc(I);   % V is the amplitude of the valley, W2 shows the index of the valley
Location = zeros(1,length(P));
M = median(I); % median of the energy envelope
temp =0; % the location of the previous peak

for i = 1:length(W1)
    if i ==1
        if  P(W1(1))> M
            NumofSyllable = NumofSyllable + 1;
            Location(W1(1)) = 1;
            temp = W1(1);
        end
    else
        if W2(1)< W1(1)
            if P(W1(i))-V(W2(i)) >thrd1 && P(W1(i))> M && W1(i)-temp>thrd2
                NumofSyllable = NumofSyllable +1;
                Location(W1(i)) = 1;
                temp = W1(i);
            end
        end
        if W2(1)>W1(1)
            if P(W1(i))-V(W2(i-1))>thrd1 && P(W1(i))> M && W1(i)-temp>thrd2
                NumofSyllable = NumofSyllable +1;
                Location(W1(i)) = 1;
                temp = W1(i);
            end
        end
    end
end

rate = NumofSyllable/Duration;





