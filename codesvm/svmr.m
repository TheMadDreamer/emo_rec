%# read dataset
[dataClass, data] = libsvmread('./DATAS.txt');

%# split into train/test datasets
trainData = data(1:30,:);
testData = data(30:43,:);
testClass = dataClass(30:43,:);
trainClass = dataClass(1:30,:);
numTrain = size(data,1);
numTest = size(testData,1);

%# radial basis function: exp(-gamma*|u-v|^2)
sigma = 2e-2;
rbfKernel = @(X,Y) exp(-sigma .* pdist2(X,Y,'euclidean').^2);
%Compute_Grassmann_Kernel(SY1,{})
%# compute kernel matrices between every pairs of (train,train) and
%# (test,train) instances and include sample serial number as first column
K =  [ (1:numTrain)' , rbfKernel(data,data) ];
%KK = [ (1:numTest)'  , rbfKernel(testData,trainData)  ];

%# train and test
model = svmtrain(dataClass, K, '-t 4 -v 5');
%[predClass, acc, decVals] = svmpredict(testClass, KK, model);

%# confusion matrix
%C = confusionmat(testClass,predClass)