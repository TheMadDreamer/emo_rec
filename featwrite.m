% Extract features for all wav files in a directory and write it to an xls
%   file name
% if xlsname is provided then features will be written on xlsname
function [] = featwrite(pathname, xlsname)
    files = dir(fullfile(pathname, '*.wav'));
    
    if (nargin == 1)
        prefix = '../Excels_14_dec/Feature_all';
        xlsname = [prefix,'_demo.xls'];
    end
    emotion = {'angry', 'happy', 'panic', 'sad', 'neutral', 'surprise'};
    
    file_names = ones(1, 1);
    files_feats = ones(1, 25);
    for ifile = 1:length(files)
        ifilename = files(ifile).name;
        %feature vector for a particular file
        featveci = featext(pathname, ifilename);
        featvec_i = proext(featveci);
        file_name_temp = cell(size(featvec_i, 1), 1);
        file_name_temp(1: length(featvec_i)) = {ifilename};
        file_names = vertcat(file_names, file_name_temp);
        files_feats = vertcat(files_feats, featvec_i);
    end
    
    file_names = file_names(2:end, :);
    files_feats = files_feats(2:end, :);
    % write title row
    titleline = {'Filename', 'Emotion', 'energy', 'energy diff', 'formant1', 'formant2', 'formant3', 'formant4', 'formant1 bw', 'formant2 bw', 'formant3 bw', 'formant4 bw', 'pitch', 'pitch diff', 'M1', ...
    'M2', 'M3', 'M4', 'M5', 'M6','M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'SRate'};

xlswrite(xlsname, titleline, 'Sheet1');
file_emos = {};
for ii = 1:length(file_names)
    parts = regexp(file_names(ii), '_|\.', 'split');
    %file_emo_temp = cell(1,1);
    %file_emo_temp(1) = parts{1}{1};
    file_emos{end+1} = parts{1}{1};
end

%audioname_all = cell(length(files),1);
%for filenum = 1:length(files)
 %   audioname_all{filenum,1} = files(filenum).name;
%end
file_emos = transpose(file_emos);
xlswrite(xlsname, file_names, 'Sheet1', 'A2');
xlswrite(xlsname, file_emos, 'Sheet1', 'B2');
xlswrite(xlsname, files_feats, 'Sheet1', 'C2');
end