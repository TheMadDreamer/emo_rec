% Feature selection using mutual information

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the mutual information between each column(corresponding to one
% feature) and the emotion label.
% Input:
% filename -> excel file containing the statistics of speech features.
% Outputs:
% avgMI_ldc_Clean.mat -> average mutual information over six emotions for 
% each feature.
% FS_LDC_Clean.mat -> feature indices ordered based on their average mutual 
% information with respect to six emotions.
% ldc_clean_top20.mat, ldc_clean_top40.mat, ldc_clean_top60.mat,
% ldc_clean_top80.mat, ldc_clean_top100.mat, ldc_clean_top121.mat -> 
% top 20/40/60/80/100/121 feature indices (already add 3 to match the columns
% in the extracted feature excel sheet)


clear all;
close all;
clc;

addpath('mi');
filename = 'FeatureExtraction2_BaNa_LDC_ASDNHF_CleanData_20130803_MFCC_norm.xls';
num_emotion = 6;
num_feature = 121;
color = ['r','b','y','g','k','m','c'];
I = xlsread(['../Excels/' filename]);
% I = I(I(:,3)==2,:);  % for gender dependent feature selection, 1 for male, 2 for female
[m,n] = size(I);

% 1 disgust 2 happy 3 anger 4 sadness 5 neutral 6 fear
I1 = find(I(:,1)==1);
I2 = find(I(:,1)==2);
I3 = find(I(:,1)==3);
I4 = find(I(:,1)==4);
I5 = find(I(:,1)==5);
I6 = find(I(:,1)==6);

z = zeros(num_emotion, num_feature);
for i = 4:124 % select feature column indices in Excel sheets
    pos = zeros(m,1);
    pos(I1,1) = 1;
    z(1,i-3) = mutualinfo(pos,I(:,i)); % calculate mutual information
end
for i = 4:124
    pos = zeros(m,1);
    pos(I2,1) = 1;
    z(2,i-3) = mutualinfo(pos,I(:,i));
end
for i = 4:124
    pos = zeros(m,1);
    pos(I3,1) = 1;
    z(3,i-3) = mutualinfo(pos,I(:,i));
end
for i = 4:124
    pos = zeros(m,1);
    pos(I4,1) = 1;
    z(4,i-3) = mutualinfo(pos,I(:,i));
end
for i = 4:124
    pos = zeros(m,1);
    pos(I5,1) = 1;
    z(5,i-3) = mutualinfo(pos,I(:,i));
end
for i = 4:124
    pos = zeros(m,1);
    pos(I6,1) = 1;
    z(6,i-3) = mutualinfo(pos,I(:,i));
end
zmax = max(z);
zmin = min(z);
zz = sum(z)/6; % calculate the average mutual information for 6 emotions
save('avgMI_ldc_Clean', 'zz');

% plot the mutual information for each emotion
figure(1);
for i = 1:6
    stem(z(i,:),color(i),'LineWidth',2);
    hold on;
end
i = i+1;
legend('disgust','happiness','anger','sadness','neutral','fear');
title('FS using MI');
% obtain the sorted feature index based on their mutual information
[B,index] = sort(zz,'descend');
index = index + 3;
save('FS_LDC_Clean','index');

top20 = sort(index(1:20));
top40 = sort(index(1:40));
top60 = sort(index(1:60));
top80 = sort(index(1:80));
top100 = sort(index(1:100));
top121 = sort(index(1:121));

save('ldc_clean_top20','top20');
save('ldc_clean_top40','top40');
save('ldc_clean_top60','top60');
save('ldc_clean_top80','top80');
save('ldc_clean_top100','top100');
save('ldc_clean_top121','top121');
