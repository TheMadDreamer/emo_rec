function [x, indx] = multi(data, label, m, num_f)
% Implementation of SMOTE upsample algorithm
% 
% Input arguments:
% data - feature matrix, each row represents the features of each sample;
% label - label vector, each row represents the label of each sample, here is 1 and others, and we only upsample data with label 1;
% m - how many times more do you want to upsample your data, for example, if you have 10 samples that being upsample to 50, m= 4 in this case;
% num_f - number of features you choose from all the features;
% 
% Output:
% x - feature matrix including all the originals and the upsampled ones;
% indx - label vector including all the originals and the upsampled ones;
%
% Modified based on N. Chawla et. al.'s original algorithm, written by 
% Jiaobo Yuan, Yun Zhou University of Rochester.
% Permission to use, copy, modify, and distribute this software without
% fee is hereby granted FOR RESEARCH PURPOSES only, provided that this
% copyright notice appears in all copies and in all supporting
% documentation, and that the software is not redistributed for any fee.
%
% For any other uses of this software, in original or modified form,
% including but not limited to consulting, production or distribution
% in whole or in part, specific prior permission must be obtained from WCNG.
% Algorithms implemented by this software may be claimed by patents owned
% by WCNG, University of Rochester.
%
% The WCNG makes no representations about the suitability of this
% software for any purpose. It is provided "as is" without express
% or implied warranty.
%
    % Initialize outputs
    x = [];
    indx = [];
    % feature matrix of the upsampled data
    y = zeros(m,num_f);
    % we want to upsample the data under label "1"
    idx = find(label==1);
    neg_idx = find(label~=1);
    % caculate distances between each positive sample
    pos_data = data(idx,:)';
    dis = dist(pos_data);
    pos_data = pos_data';
    % for each positive sample, find the nearest m neighbors, and create samples between this sample and its neighbors 
    for i = 1:length(dis)
        x = [x;pos_data(i,:)];
        indx = [indx;1];
        [d,index] = sort(dis(:,i));
        for j = 1 : m
            y(j,:) = pos_data(i,:) + (pos_data(index(j+1),:) - pos_data(i,:)).*rand(1);        
        end
        x = [x;y];
        indx = [indx;ones(m,1)];
    end
    x = [x;data(neg_idx,:)];
    indx = [indx;label(neg_idx)];
end