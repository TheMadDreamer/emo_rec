% Decision Level Leave-One-Subject-Out Test for Each Speaker Using Feature Selection and Upsampling
%
% See the Bridge project website at Wireless Communication and Networking
% Group (WCNG), University of Rochester for more information: 
% http://www.ece.rochester.edu/projects/wcng/project_bridge.html
%
% Written by Jianbo Yuan, Yun Zhou University of Rochester.
% Copyright (c) 2014 University of Rochester.
% Version: June 2014.
%
% Permission to use, copy, modify, and distribute this software without 
% fee is hereby granted FOR RESEARCH PURPOSES only, provided that this
% copyright notice appears in all copies and in all supporting 
% documentation, and that the software is not redistributed for any fee.
%
% For any other uses of this software, in original or modified form, 
% including but not limited to consulting, production or distribution
% in whole or in part, specific prior permission must be obtained from WCNG.
% Algorithms implemented by this software may be claimed by patents owned 
% by WCNG, University of Rochester.
%
% The WCNG makes no representations about the suitability of this 
% software for any purpose. It is provided "as is" without express
% or implied warranty.
%

clear all;
close all;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Parameter settings
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Absolute confidence threshold setting. If the highest confidence scores are 
% smaller than 'thresh', the sample is left to be unclassified. A larger 
% 'thresh' value can increase the emotion classification accuracy at the 
% expense of rejecting more samples as unclassified.

featureSet = [20,40,60,80,100,121]; % number of features selected

for m = 1:length(featureSet)
    
    thresh = -2.0:0.05:2.0; % absolute confidence threshold
    cnt_thresh = length(thresh);
    no_of_class = 6; % number of emotion classes
    % select feature column indices in Excel sheets
    featureMat = ['ldc_clean_top' num2str(featureSet(m))];
    load(featureMat);
    feature = eval(['top' num2str(featureSet(m))]);
    dataset = 'notused';
    subject = {'cc', 'cl', 'gg', 'jg', 'mf', 'mk', 'mm'}; % 7 speakers
    num_speaker = length(subject);
    num_trials = 5; % number of trials for upsampling
    trainfilepath = '../Datasets/Dataset_LDC_Clean_norm_backup/LOSO_train_norm/'; % path where the training data is located
    testfilepath = '../Datasets/Dataset_LDC_Clean_norm_backup/LOSO_test_norm/'; % path where the testing data is located
    xlsname = '../Excels/raw_result_loso.xls'; % an output Excel file containing the result
    sheet = ['Sheet' num2str(m)];
    offset = 1; % which line to write in the output excel file

    % store the result for changing threshold for each speaker
    % 1 - absolute confidence threshold, 2 - rejection rate, 3 - correct classification rate
    Thresh_accu{num_speaker,1} = zeros(3,cnt_thresh);

    % Decision-level recall for each individual emotion - happy, sad, fear, 
    % anger, disgust, neutral
    recall_emotion = zeros(num_speaker, no_of_class);
    % the number of instances corresponding to each speaker for each emotion 
    % (anger, sad, disgust, neutral, happy, fear)
    num_emotion = zeros(num_speaker, no_of_class);

    for time = 1:num_trials % number of trials for upsampling
        disp([num2str(time) ' upsampling trial']);
        % LOSO test for each speaker
        for cnt = 1:num_speaker
            disp(['Running LOSO for speaker ' subject{cnt} '...']);
            disp('------------------------------');
            [Thresh_accu{cnt,1}, recall_emotion(cnt,:), num_emotion(cnt,:)] = OnefoldCV(trainfilepath, ...
                testfilepath, feature, thresh, dataset, cnt, subject);
        end
        % Calculate the WEIGHTED average recall for each emotion over k speakers -anger, sad, disgust, neutral, happy, fear
        avg_recall_emotion = sum(recall_emotion.*num_emotion)./sum(num_emotion);
        % Calculate average rejection rate, correct classification rate over k
        % speakers
        if cnt_thresh == 1
            avg_rej_rate = zeros(1,cnt_thresh);
            avg_cor_rate = zeros(1,cnt_thresh);
            for myi = 1:cnt_thresh
                for cnt = 1:num_speaker
                    avg_rej_rate(myi) = avg_rej_rate(myi)+Thresh_accu{cnt,1}(2,myi);
                    avg_cor_rate(myi) = avg_cor_rate(myi)+Thresh_accu{cnt,1}(3,myi);
                end
            end
            avg_rej_rate = avg_rej_rate./num_speaker;
            avg_cor_rate = avg_cor_rate./num_speaker;
        else
            % construct a (2*num_speaker) * cnt_thresh matrix with (2*j-1)th row being the rejection rate,
            % (2*j)th row being the correct classification rate (j = 1:7)
            rej_cor = zeros(2*num_speaker, cnt_thresh);
            for cnt = 1:num_speaker
                rej_cor(2*cnt-1,:) = Thresh_accu{cnt,1}(2,:);
                rej_cor(2*cnt,:) = Thresh_accu{cnt,1}(3,:);
            end
            % apply linear interpolation to obtain the average rejection rate and correct
            % classification rate
            [avg_rej_rate, avg_cor_rate] = interp(rej_cor, num_speaker);
        end

        % plot decision-level LOSO rejection rate vs. correct classification rate
        % averaged over all speakers
        fig1 = figure;
        hold on;
        plot(Thresh_accu{1,1}(2,:),Thresh_accu{1,1}(3,:),'-dr','LineWidth',2);
        plot(Thresh_accu{2,1}(2,:),Thresh_accu{2,1}(3,:),'-^c','LineWidth',2); 
        plot(Thresh_accu{3,1}(2,:),Thresh_accu{3,1}(3,:),'-<b','LineWidth',2); 
        plot(Thresh_accu{4,1}(2,:),Thresh_accu{4,1}(3,:),'-sm','LineWidth',2); 
        plot(Thresh_accu{5,1}(2,:),Thresh_accu{5,1}(3,:),'-+g','LineWidth',2); 
        plot(Thresh_accu{6,1}(2,:),Thresh_accu{6,1}(3,:),'->k','LineWidth',2); 
        plot(Thresh_accu{7,1}(2,:),Thresh_accu{7,1}(3,:),'-py','LineWidth',2); 

        plot(avg_rej_rate,avg_cor_rate,'-ob','LineWidth',2,'MarkerSize',10);
        legend('cc', 'cl', 'gg', 'jg', 'mf', 'mk', 'mm', 'Average');
        h1 = xlabel('Rejection rate (%)'); 
        h2 = ylabel('Decision-level correct classification rate (%)');
        set(h1,'fontsize',12);
        set(h2,'fontsize',12);
        set(gca,'fontsize',12);
        xlim([0 80]);
        ylim([20 100]);
        title('LDC, LOSO, up, abs');
        grid on;
        pause(0.2);
        
        % % Write to an excel file to store the result:
        % 1st row. absolute confidence threshold, 2nd row. average rejection rate, 
        % 3rd row. correct classification rate
        warning off MATLAB:xlswrite:AddSheet
        xlswrite(xlsname, thresh, sheet, sprintf('A%d',offset));
        offset = offset+1;
        xlswrite(xlsname, avg_rej_rate, sheet, sprintf('A%d',offset));
        offset = offset+1;
        xlswrite(xlsname, avg_cor_rate, sheet, sprintf('A%d',offset));
        offset = offset+1;

        % Display average rejection rate and correct classification rate for each
        % absolute confidence threshold
        disp('Absolute threshold:');
        disp(thresh);
        disp('Avg rejection rate:');
        disp(avg_rej_rate);
        disp('Correct classification rate:');
        disp(avg_cor_rate);
        % Display DL recall for each individual emotion
        if cnt_thresh == 1
            disp('Recall for each emotion:');
            fprintf('\tanger\t\tsad\t\tdisgust\tneutral\t\thappy\tfear\n');
            disp(avg_recall_emotion);
        end
        disp('-------------------------------------------------------------');
    end
    close all;
    disp([num2str(featureSet(m)) ' feature DONE']);
    fprintf('\n');
end