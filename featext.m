%Extract features of a particular file and returns the feature vector
%input file can also be read earlier and passed through inputx and inputfs
%       earlier 2 parameters will be ignored in such a case
function [features] = featext(pathname, filename, inputx, inputfs)

emotion = {'disgust', 'happy', 'anger', 'sadness', 'neutral', 'panic'}; %  emotion categories, consistent with those in the audio names

% Use the VOICEBOX for MFCCs calculation
path(path,'voicebox');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Parameter settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
frame_dur = 0.06; % frame duration is 60 ms
advance_dur = 0.01; % frame step size is 10 ms
f0max = 600; % higher bound of human speech pitch (Hz)
f0min = 50; % lower bound of human speech pitch (Hz)
pthr1 = 2; % amplitude ratio of the first two highest Cepstrum peaks, used 
% for voiced/unvoiced detection. pthr1 is a value greater than 1, a higher 
% value provides a more stringent voiced segment detection.


% initialize feature values for each frame of each utterance
energy = cell(1,1); % energy
energy_diff = cell(1,1); % energy difference
pitch = cell(1,1); % pitch
pitch_diff = cell(1,1); % pitch difference
formant = cell(1,4); % frequency for the first 4 formants
ffreq_bw = cell(1,4); % bandwidth for the first 4 formants
MFCC = cell(1,12); % 12 MFCCs
SRate = cell(1,1); % speaking rate

% Feature extraction starts
for filenum = 1:1%length(files)
    %filename = files(filenum).name;
    if (nargin <= 2)
        [x,fss] = wavread([pathname filename]);
        %%%%%%%%%%% Resampling %%%%%%%%%%%
        [P, Q] = rat(8000/fss);
        x = resample(x, P, Q);
        fs = 8000;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    if (nargin > 2)
        x = inputx;
        fs = inputfs;
    end
    if (size(x,1)<size(x,2))
        x=x';
    end
    % If the audio file has two channels, choose the channel with
    % better signal quality (higher total energy)
    if size(x,2) == 2
        total = zeros(1,2);
        for ch = 1:2
            temp = 0;
            for myi = 1:size(x,1)
                temp = temp + x(myi,ch)^2;
            end
            total(ch) = temp;
        end
        [~, channel] = max(total);
        x = x(:,channel);
    end
    
    % frame settings
    frame_len = ceil(frame_dur*fs); % number of samples per frame
    advance_len = ceil(advance_dur*fs);
    frame_num = floor((length(x)-frame_len)/advance_len)+1; % number of frames
    
    % sample marker
    thr_amp = 0.02; % absolute threshold of amplitude (clean LDC: 0.02 is noise level)
    sample_marker = zeros(1,length(x));
    sample_marker(abs(x)>thr_amp) = 1;
    
    % frame marker
    thr_ratio = 0.2; % threshold of percentage of sample marker=1 (CD: 0.2)
    frame_marker = zeros(1,frame_num);
    for frame_index = 1:frame_num
        head = uint32((frame_index-1)*advance_len+1);
        tail = uint32((frame_index-1)*advance_len+frame_len);
        if nnz(sample_marker(head:tail))/frame_len > thr_ratio
            frame_marker(frame_index) = 1;
        end
    end
    
    % energy
    % calculate energy for all frames
    energy{filenum} = NaN.*ones(frame_num,1);
    for frame_index = 1:frame_num
        if frame_marker(frame_index) == 1
            head = uint32((frame_index-1)*advance_len+1);
            tail = uint32((frame_index-1)*advance_len+frame_len);
            x1 = x(head:tail,1);
            energy{filenum}(frame_index) = sum(x1'.*x1');
        end
    end
    energy_diff{filenum} = diff(energy{filenum});
    
    % formant frequency and formant bandwidth (first 4)
    % (reference: http://www.phon.ucl.ac.uk/courses/spsci/matlab/lect10.html)
    for ii = 1:4
        formant{filenum,ii} = NaN.*ones(frame_num,1); % formant frequency
        ffreq_bw{filenum,ii} = NaN.*ones(frame_num,1); % formant bandwidth
    end;
    for frame_index = 1:frame_num
        if frame_marker(frame_index) == 1 % compute formant only if frame marker is 1
            head = uint32((frame_index-1)*advance_len+1);
            tail = uint32((frame_index-1)*advance_len+frame_len);
            x1 = x(head:tail,1);
            
            % resample to 10,000Hz (optional)
            x1=resample(x1,10000,fs);
            fs1=10000;
            
            % get linear prediction filter
            ncoeff=2+fs1/1000;           % rule of thumb for formant estimation
            a=lpc(x1,ncoeff);
            
            % frequency response (gain vs. f)
            [h,f]=freqz(1,a,512,fs1);
            gain = 20*log10(abs(h)+eps);
            
            % find frequencies by root-solving
            r=roots(a);                  % find roots of polynomial a
            r=r(imag(r)>0.01);           % only look for roots >0Hz up to fs/2
            ffreq=sort(atan2(imag(r),real(r))*fs1/(2*pi)); % convert to Hz and sort
            if numel(ffreq) >= 4
                for ii = 1:4
                    formant{filenum,ii}(frame_index) = ffreq(ii); % formant frequency
                end;
                
                % formant bandwidth
                ffreq_index = zeros(1,4);
                peak = zeros(1,4);
                for ii = 1:4
                    [~,ffreq_index(ii)] = min(abs(f-ffreq(ii))); % index of each formant frequency
                end;
                for ii = 1:4
                    peak(ii) = interp1(f,gain,ffreq(ii)); % gain at each formant
                    
                    f_index = ffreq_index(ii); % search from formant frequency to the left for f_low
                    if ii == 1
                        stop = 1;
                    else
                        stop = ffreq_index(ii-1);
                    end;
                    while((gain(f_index)>(peak(ii)-1)) && (f_index~=stop))
                        f_index = f_index-1;
                    end;
                    if f_index~=stop
                        f_low = f(f_index);
                    else
                        f_low = NaN;
                    end;
                    
                    f_index = ffreq_index(ii); % search from formant frequency to the right for f_high
                    if ii == 4
                        stop = 512;
                    else
                        stop = ffreq_index(ii+1);
                    end;
                    while((gain(f_index)>(peak(ii)-1)) && (f_index~=stop))
                        f_index = f_index+1;
                    end;
                    if f_index~=stop
                        f_high = f(f_index);
                    else
                        f_high = NaN;
                    end;
                    ffreq_bw{filenum,ii}(frame_index) = f_high - f_low;
                end
            end
        end
    end
    
    
    % pitch detection using BaNa, a noise-resilient pitch detection
    % algorithm
    f0_bana = Bana_auto(x, fs, f0min, f0max, advance_dur, pthr1);
    pitch{filenum} = f0_bana;
    for myi = 1:length(pitch{filenum})
        if pitch{filenum}(myi) == 0
            pitch{filenum}(myi) = NaN;
        end
    end
    pitch_diff{filenum} = diff(pitch{filenum});
    
    
    % Calculate 12 MFCCs
    % VOICEBOX: 
    for ii = 1:12
        MFCC{filenum,ii} = NaN.*ones(frame_num,1); % formant frequency
    end
    w = ''; % do not include delta values
    nc = 12;
    p = floor(3*log(fs));
    n = round(frame_dur*fs);
    inc = round(0.01*fs);
    fl = 0;
    fh = 0.5;
    c=melcepst(x,fs,w,nc,p,n,inc,fl,fh);
    for ii = 1:12
        MFCC{filenum,ii}(:) = c(:,ii);
    end
    
    
    % Caculate speaking rate (number of syllables per second)
    if (nargin > 2)
        [~,~,~,~,SRate{filenum},~,~] = speechrate([pathname filename],'moreband', inputx, inputfs);  
    end
    if (nargin <= 2)
        [~,~,~,~,SRate{filenum},~,~] = speechrate([pathname filename],'moreband');  
    end
    
    if mod(filenum,10) == 0;
        disp([num2str(filenum) ' out of ' num2str(1) ' files have been processed.']);
    end    
end

features_notnorm = [energy energy_diff formant ffreq_bw pitch pitch_diff MFCC SRate];
features = features_notnorm;
disp('Feature extraction finished.');

% Code used from :
% http://www.ece.rochester.edu/projects/wcng/project_bridge.html
% Written by He Ba and Na Yang, University of Rochester.
% Cepstrum code provided by Lawrence R. Rabiner, Rutgers University and
% University of California at Santa Barbara.
% Copyright (c) 2013 University of Rochester.
% Version March 2013*.
% Permission to use, copy, modify, and distribute this software without 
% fee is hereby granted FOR RESEARCH PURPOSES only, provided that this
% copyright notice appears in all copies and in all supporting 
% documentation, and that the software is not redistributed for any fee.
%
% *December 2015
% Modified by Pulkit Sharma, IIT Delhi
%