function [ features ] = featureextraction( pathname )
prefix = '../Excels/Feature_demo';
xlsname_raw_notnorm = [prefix,'_raw_notnorm.xls'];
xlsname_notnorm = [prefix,'_notnorm1.xls'];
audioFiles_dir = pathname;%'../Audio/testset/';
emotion = {'angry', 'happy', 'panic', 'sad', 'neutral', 'surprise'}; %  emotion categories, consistent with those in the audio names

% Use the VOICEBOX for MFCCs calculation
path(path,'voicebox');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Parameter settings
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
frame_dur = 0.06; % frame duration is 60 ms
advance_dur = 0.01; % frame step size is 10 ms
f0max = 600; % higher bound of human speech pitch (Hz)
f0min = 50; % lower bound of human speech pitch (Hz)
pthr1 = 2; % amplitude ratio of the first two highest Cepstrum peaks, used 
% for voiced/unvoiced detection. pthr1 is a value greater than 1, a higher 
% value provides a more stringent voiced segment detection.

% Read clean speech/music samples
files = dir(fullfile(audioFiles_dir,'*.wav'));

% initialize feature values for each frame of each utterance
energy = cell(length(files),1); % energy
energy_diff = cell(length(files),1); % energy difference
pitch = cell(length(files),1); % pitch
pitch_diff = cell(length(files),1); % pitch difference
formant = cell(length(files),4); % frequency for the first 4 formants
ffreq_bw = cell(length(files),4); % bandwidth for the first 4 formants
MFCC = cell(length(files),12); % 12 MFCCs
SRate = cell(length(files),1); % speaking rate

% Feature extraction starts
for filenum = 1:length(files)
    filename = files(filenum).name;
    [x,fs] = wavread([audioFiles_dir filename]);
    if (size(x,1)<size(x,2))
        x=x';
    end
    % If the audio file has two channels, choose the channel with
    % better signal quality (higher total energy)
    if size(x,2) == 2
        total = zeros(1,2);
        for ch = 1:2
            temp = 0;
            for myi = 1:size(x,1)
                temp = temp + x(myi,ch)^2;
            end
            total(ch) = temp;
        end
        [~, channel] = max(total);
        x = x(:,channel);
    end
    
    % frame settings
    frame_len = ceil(frame_dur*fs); % number of samples per frame
    advance_len = ceil(advance_dur*fs);
    frame_num = floor((length(x)-frame_len)/advance_len)+1; % number of frames
    
    % sample marker
    thr_amp = 0.02; % absolute threshold of amplitude (clean LDC: 0.02 is noise level)
    sample_marker = zeros(1,length(x));
    sample_marker(abs(x)>thr_amp) = 1;
    
    % frame marker
    thr_ratio = 0.2; % threshold of percentage of sample marker=1 (CD: 0.2)
    frame_marker = zeros(1,frame_num);
    for frame_index = 1:frame_num
        head = uint32((frame_index-1)*advance_len+1);
        tail = uint32((frame_index-1)*advance_len+frame_len);
        if nnz(sample_marker(head:tail))/frame_len > thr_ratio
            frame_marker(frame_index) = 1;
        end
    end
    
    % energy
    % calculate energy for all frames
    energy{filenum} = NaN.*ones(frame_num,1);
    for frame_index = 1:frame_num
        if frame_marker(frame_index) == 1
            head = uint32((frame_index-1)*advance_len+1);
            tail = uint32((frame_index-1)*advance_len+frame_len);
            x1 = x(head:tail,1);
            energy{filenum}(frame_index) = sum(x1'.*x1');
        end
    end
    energy_diff{filenum} = diff(energy{filenum});
    
    % formant frequency and formant bandwidth (first 4)
    % (reference: http://www.phon.ucl.ac.uk/courses/spsci/matlab/lect10.html)
    for ii = 1:4
        formant{filenum,ii} = NaN.*ones(frame_num,1); % formant frequency
        ffreq_bw{filenum,ii} = NaN.*ones(frame_num,1); % formant bandwidth
    end;
    for frame_index = 1:frame_num
        if frame_marker(frame_index) == 1 % compute formant only if frame marker is 1
            head = uint32((frame_index-1)*advance_len+1);
            tail = uint32((frame_index-1)*advance_len+frame_len);
            x1 = x(head:tail,1);
            
            % resample to 10,000Hz (optional)
            x1=resample(x1,10000,fs);
            fs1=10000;
            
            % get linear prediction filter
            ncoeff=2+fs1/1000;           % rule of thumb for formant estimation
            a=lpc(x1,ncoeff);
            
            % frequency response (gain vs. f)
            [h,f]=freqz(1,a,512,fs1);
            gain = 20*log10(abs(h)+eps);
            
            % find frequencies by root-solving
            r=roots(a);                  % find roots of polynomial a
            r=r(imag(r)>0.01);           % only look for roots >0Hz up to fs/2
            ffreq=sort(atan2(imag(r),real(r))*fs1/(2*pi)); % convert to Hz and sort
            if numel(ffreq) >= 4
                for ii = 1:4
                    formant{filenum,ii}(frame_index) = ffreq(ii); % formant frequency
                end;
                
                % formant bandwidth
                ffreq_index = zeros(1,4);
                peak = zeros(1,4);
                for ii = 1:4
                    [~,ffreq_index(ii)] = min(abs(f-ffreq(ii))); % index of each formant frequency
                end;
                for ii = 1:4
                    peak(ii) = interp1(f,gain,ffreq(ii)); % gain at each formant
                    
                    f_index = ffreq_index(ii); % search from formant frequency to the left for f_low
                    if ii == 1
                        stop = 1;
                    else
                        stop = ffreq_index(ii-1);
                    end;
                    while((gain(f_index)>(peak(ii)-1)) && (f_index~=stop))
                        f_index = f_index-1;
                    end;
                    if f_index~=stop
                        f_low = f(f_index);
                    else
                        f_low = NaN;
                    end;
                    
                    f_index = ffreq_index(ii); % search from formant frequency to the right for f_high
                    if ii == 4
                        stop = 512;
                    else
                        stop = ffreq_index(ii+1);
                    end;
                    while((gain(f_index)>(peak(ii)-1)) && (f_index~=stop))
                        f_index = f_index+1;
                    end;
                    if f_index~=stop
                        f_high = f(f_index);
                    else
                        f_high = NaN;
                    end;
                    ffreq_bw{filenum,ii}(frame_index) = f_high - f_low;
                end
            end
        end
    end
    
    
    % pitch detection using BaNa, a noise-resilient pitch detection
    % algorithm
    f0_bana = Bana_auto(x, fs, f0min, f0max, advance_dur, pthr1);
    pitch{filenum} = f0_bana;
    for myi = 1:length(pitch{filenum})
        if pitch{filenum}(myi) == 0
            pitch{filenum}(myi) = NaN;
        end
    end
    pitch_diff{filenum} = diff(pitch{filenum});
    
    
    % Calculate 12 MFCCs
    % VOICEBOX: 
    for ii = 1:12
        MFCC{filenum,ii} = NaN.*ones(frame_num,1); % formant frequency
    end
    w = ''; % do not include delta values
    nc = 12;
    p = floor(3*log(fs));
    n = round(0.06*fs);
    inc = round(0.01*fs);
    fl = 0;
    fh = 0.5;
    c=melcepst(x,fs,w,nc,p,n,inc,fl,fh);
    for ii = 1:12
        MFCC{filenum,ii}(:) = c(:,ii);
    end
    
    
    % Caculate speaking rate (number of syllables per second)
    [~,~,~,~,SRate{filenum},~,~] = speechrate([audioFiles_dir filename],'moreband');  
    
    if mod(filenum,10) == 0;
        disp([num2str(filenum) ' out of ' num2str(length(files)) ' files have been processed.']);
    end    
end

features_notnorm = [energy energy_diff formant ffreq_bw pitch pitch_diff MFCC SRate];
disp('Feature extraction finished.');

%% Calculate statistical values for normalized/ not normalized features
statistics_raw_notnorm = NaN*ones(length(files),(size(features_notnorm,2)-1)*5+1);
statistics_raw_norm = statistics_raw_notnorm;
for filenum = 1:length(files)
    for myf = 1:size(features_notnorm,2)-1
        tmp = features_notnorm{filenum,myf};
        statistics_raw_notnorm(filenum,5*(myf-1)+1:5*myf) = [nanmean(tmp) nanstd(tmp) nanmax(tmp) nanmin(tmp) nanmax(tmp)-nanmin(tmp)];
        
        %tmp_norm = features_norm{filenum,myf};
        %statistics_raw_norm(filenum,5*(myf-1)+1:5*myf) = [nanmean(tmp_norm) nanstd(tmp_norm) nanmax(tmp_norm) nanmin(tmp_norm) nanmax(tmp_norm)-nanmin(tmp_norm)];
    end
    
    % add speaking rate
    statistics_raw_notnorm(filenum,(size(features_notnorm,2)-1)*5+1) = features_notnorm{filenum,size(features_notnorm,2)};
    %statistics_raw_norm(filenum,(size(features_norm,2)-1)*5+1) = features_norm{filenum,size(features_norm,2)};
end
disp('Statistical values calculation finished.');
features = statistics_raw_notnorm;

%% Write xlsname_raw_notnorm
delete(xlsname_raw_notnorm);

% write title row
titleline = {'Filename', 'Emotion', 'PositiveNegative', 'Gender',...
    'mean energy', 'std energy', 'max energy', 'min energy', 'range energy',...
    'mean energy diff', 'std energy diff', 'max energy diff', 'min energy diff', 'range energy diff',...
    'mean formant1', 'std formant1', 'max formant1', 'min formant1', 'range formant1',...
    'mean formant2', 'std formant2', 'max formant2', 'min formant2', 'range formant2',...
    'mean formant3', 'std formant3', 'max formant3', 'min formant3', 'range formant3',...
    'mean formant4', 'std formant4', 'max formant4', 'min formant4', 'range formant4',...
    'mean formant1 bw', 'std formant1 bw', 'max formant1 bw', 'min formant1 bw', 'range formant1 bw',...
    'mean formant2 bw', 'std formant2 bw', 'max formant2 bw', 'min formant2 bw', 'range formant2 bw',...
    'mean formant3 bw', 'std formant3 bw', 'max formant3 bw', 'min formant3 bw', 'range formant3 bw',...
    'mean formant4 bw', 'std formant4 bw', 'max formant4 bw', 'min formant4 bw', 'range formant4 bw',...
    'mean pitch', 'std pitch', 'max pitch', 'min pitch', 'range pitch',...
    'mean pitch diff', 'std pitch diff', 'max pitch diff', 'min pitch diff', 'range pitch diff',...
    'mean M1', 'std M1', 'max M1', 'min M1', 'range M1', ...
    'mean M2', 'std M2', 'max M2', 'min M2', 'range M2', ...
    'mean M3', 'std M3', 'max M3', 'min M3', 'range M3', ...
    'mean M4', 'std M4', 'max M4', 'min M4', 'range M4', ...
    'mean M5', 'std M5', 'max M5', 'min M5', 'range M5', ...
    'mean M6', 'std M6', 'max M6', 'min M6', 'range M6', ...
    'mean M7', 'std M7', 'max M7', 'min M7', 'range M7', ...
    'mean M8', 'std M8', 'max M8', 'min M8', 'range M8', ...
    'mean M9', 'std M9', 'max M9', 'min M9', 'range M9', ...
    'mean M10', 'std M10', 'max M10', 'min M10', 'range M10', ...
    'mean M11', 'std M11', 'max M11', 'min M11', 'range M11', ...
    'mean M12', 'std M12', 'max M12', 'min M12', 'range M12', ...
    'SRate'};
xlswrite(xlsname_raw_notnorm, titleline, 'Sheet1');

% write audio file names
audioname_all = cell(length(files),1);
for filenum = 1:length(files)
    [a, b, c] = fileparts(files(filenum).name);
    audioname_all{filenum,1} = b;
end
xlswrite(xlsname_raw_notnorm, audioname_all, 'Sheet1', 'A2');

% write statistical values
xlswrite(xlsname_raw_notnorm, statistics_raw_notnorm, 'Sheet1', 'B2');

%% Write xlsname_notnorm
% Delete lines with empty entries (NaN)
NaN_indicator = isnan(statistics_raw_notnorm);
[rows,~,~] = find(NaN_indicator == 1);
if numel(rows)~=0
    rows = unique(rows);
end
kept_lines = setxor(rows, 1:length(files)); % kept_lines in xlsname_notnorm and xlsname_norm are the same

delete(xlsname_notnorm);

% write title row
xlswrite(xlsname_notnorm, titleline, 'Sheet1');

% write audio file names
audioname_kept = audioname_all(kept_lines);
xlswrite(xlsname_notnorm, audioname_kept, 'Sheet1', 'A2');

% write statistical values
xlswrite(xlsname_notnorm, statistics_raw_notnorm(kept_lines,:), 'Sheet1', 'B2');
