function energy = crossenergy_moreband(sig, win,overlap,fs)
% Written by Weiyang Cai, University of Rochester.
% Generate the energy envelop of the subband M=19 method
winLen = length(win);

% Framing the signal without for loops
[sigFramed, lastFrame] = buffer(sig, winLen, overlap, 'nodelay');
sigWindowed = diag(sparse(win)) * sigFramed;

dim = size(sigWindowed);
energy = zeros(1,dim(2));
N = 2^16;
for i = 1:dim(2);
    Y = abs(fft(sigWindowed(:,i),N));
    % boundary of each sub-band
    I1 =floor(N*100/fs);
    I2 =floor(N*260/fs);
    I3 =floor(N*400/fs);
    I4 =floor(N*440/fs);
    I5 =floor(N*620/fs);
    I6 =floor(N*800/fs);
    I7 =floor(N*980/fs);
    I8 =floor(N*1160/fs);
    I9 =floor(N*1340/fs);
    I10=floor(N*1520/fs);
    I11=floor(N*1700/fs);
    I12=floor(N*1880/fs);
    I13=floor(N*2060/fs);
    I14=floor(N*2240/fs);
    I15=floor(N*2420/fs);
    I16=floor(N*2600/fs);
    I17=floor(N*2780/fs);
    I18=floor(N*2960/fs);
    I19=floor(N*3560/fs);
    I20=floor(N*4400/fs);
    I21=floor(N*5480/fs);
    I22=floor(N*6560/fs);
    %Energy in each sub-band
    subband1 = sum(Y(I1:I3-1).^2)/N; % 100 400
    subband2 = sum(Y(I2:I5-1).^2)/N; % 260 620
    subband3 = sum(Y(I4:I6-1).^2)/N; % 440 800
    subband4 = sum(Y(I5:I7-1).^2)/N; % 620 980
    subband5 = sum(Y(I6:I8-1).^2)/N; % 800 1160
    subband6 = sum(Y(I7:I9-1).^2)/N; % 980 1340
    subband7 = sum(Y(I8:I10-1).^2)/N; % 1160 1520
    subband8 = sum(Y(I9:I11-1).^2)/N; % 1340 1700
    subband9 = sum(Y(I10:I12-1).^2)/N; % 1520 1880
    subband10 = sum(Y(I11:I13-1).^2)/N;% 1700 2060
    subband11 = sum(Y(I12:I14-1).^2)/N; % 1880 2240
    subband12 = sum(Y(I13:I15-1).^2)/N;% 2060 2420
    subband13 = sum(Y(I14:I16-1).^2)/N;% 2240 2600
    subband14 = sum(Y(I15:I17-1).^2)/N;% 2420 2780
    subband15 = sum(Y(I16:I18-1).^2)/N;% 2600 2960
    subband16 = sum(Y(I18:I19-1).^2)/N;% 2960 3560
    subband17 = sum(Y(I19:I20-1).^2)/N; % 3560 4400
    subband18 = sum(Y(I20:I21-1).^2)/N;% 4400 5480
    subband19 = sum(Y(I21:I22-1).^2)/N; % 5480 6560
    
    band = [subband1 subband2 subband3 subband4 subband5 subband6 subband7 subband8 subband9 subband10 subband11 subband12 subband13 subband14 subband15 subband16 subband17 subband18 subband19];
    sortedband = sort(band,'descend');
    %select the 12 sub-band with largest energy
    numofbands = 12;
    selectedband = sortedband(1:numofbands);
    M = zeros(numofbands,numofbands);
    for j = 1:numofbands
        M(j,:) = selectedband(j);
    end
    M = triu(M,1);
    R = M*selectedband';
    energy(i) = sum(R)/(numofbands*(numofbands-1)/2); % 11(11+1)/2 = 66
end

