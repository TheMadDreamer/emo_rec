function [featvec] = proext(feats)
%convert the daa from cell array to double array
    l = length(cell2mat(feats(1)));
    kans = NaN*ones(l, length(feats));
    for i = 1:(length(feats) - 1)
        x = cell2mat(feats(i));
        lb = length(x);
        if (lb ~= l)
            x_temp = NaN*ones(abs(lb - l), 1);
            x = vertcat(x, x_temp);
        end
        kans(:, i) = x;
    end
    kans(:, length(feats)) = cell2mat(feats(length(feats)));
    featvec = kans;
end