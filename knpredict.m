% input c and x
function [index] = knpredict(c, x)
sx = size(x);
index = ones(sx(1),1);
for jj = 1:sx(1)
    min = Inf;
    for ii = 1:size(c, 1)
        a = x(jj,:) - c(ii, :);
        b = norm(a);
        if (b <= min)
            min = b;
            index(jj) = ii;
        end
    end
end
end